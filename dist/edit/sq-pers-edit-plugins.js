/* base js 1 */

/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

EasyEdit.plugins.BooleanSwitcher = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initBooleanSwitcher);

	},// End init

	// Hide what's required and listen for changes
	initBooleanSwitcher: function() {

		if( $('.onoffswitch').length > 0 ) {

			// For each instance of the switch ...
			$.each( $('.onoffswitch'), function( key, instance ) {

				// Set the labels
				var onLabel = $(instance).data('on-txt');
				var offLabel = $(instance).data('off-txt');

				// Tweak the markup
				$(instance).find('label').empty();
				$(instance).find('label').append('<span data-txt-off="' + offLabel + '" data-txt-on="' + onLabel + '" class="onoffswitch-inner"></span>');
				$(instance).find('label').append('<span class="onoffswitch-switch"></span>');

				var sweetSwitch = $(instance).html();

				sweetSwitch = sweetSwitch.replace('<ul>', '');
				sweetSwitch = sweetSwitch.replace('<li>', '');
				sweetSwitch = sweetSwitch.replace('</li>', '');
				sweetSwitch = sweetSwitch.replace('</ul>', '');

				$(instance).html( sweetSwitch );
			});
		}
	}
}
/**
 * Conditionally show/hide fields
 * Put data-listener="identifier" on the parent element
 * Put data-show="indetifier@value" on the parent element that you want to show
 * Put data-hide="indetifier@value" on the parent element that you want to hide
 */

EasyEdit.plugins.conditionalFields = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initConditionalFields);

	},// End init

	// Hide what's required and listen for changes
	initConditionalFields: function() {

		// Get all the instances of the templates on the page
		var instances = $('.matrix-options-template');

		// For each instance run the function within its scope
		$.each(instances, function( key, instance ) {
			// Execute the functions per instance
			doTheStuff( $(instance) );

		});

		// TODO: Need better name
		function doTheStuff( instance ) {

			var currentInstance = $(instance);

			// Get the listeners
			var listeners = $(currentInstance).find('[data-listener]');

			// Run actions for the listeners
			$.each(listeners, function(key, value) {

				var greaterThan = [];

				// Get the type
				var type = $(value).data('listener');

				// The items that are greater than
				var greaterItems = $(currentInstance).find('[data-show^="' + type + ':"]');

				// Get the selected value
				var selectedNow = $(value).find('input[type="text"], input[type="checkbox"]:checked, select').val();

				if( typeof selectedNow !== 'undefined' ) {

					selectedNow.toLowerCase().replace(' ','-');
				}

				// If it's a greater than listener type use the text not the value

				// NOTICE THE .TEXT() IT'S GETTING THE TEXT NOT THE VALUE ( CONSIDER .VAL() )
				if( type.indexOf('^gte') !== -1 ) {

					selectedNow = $(value).find('input[type="text"], input[type="checkbox"]:checked, select option:selected').text();
				}


				// Greater than show the fields
				// TODO: this could be extracted into function as it's simimlar in change event
				// ====================
				if( $.isNumeric( selectedNow ) ) {

					// Make any array of the greater than values
					$.each( greaterItems, function() {

						// Get just the number in the data attr
						var theNumber = $(this).data('show').substr( $(this).data('show').indexOf('^gte:') );

						theNumber = theNumber.replace('^gte:','');

						// Make into numbers
						theNumber = parseInt(theNumber);
						selectedNow = parseInt(selectedNow);

						// For the items that are greater than or equal to selected
						if( selectedNow >= theNumber ) {
							// Put in an array
							greaterThan.push( $(this).data('show') );
						}
					});

					// Hide them all
					$(currentInstance).find('[data-show^="' + type + ':"]').addClass('x-hide');

					// Show the ones that are greater than or equal to the input
					$.each( greaterThan, function( index ) {

						$(currentInstance).find( '[data-show="' + greaterThan[index] + '"]' ).removeClass('x-hide');
					});
				}

				// Hide the items
				// ====================
				// Show all
				$(currentInstance).find('[data-hide^="' + type + '@"]').removeClass('x-hide');
				// Hide items that are equal to assigned value
				$(currentInstance).find('[data-hide="' + type + '@' + selectedNow + '"]').addClass('x-hide');

				// Show the items
				// ====================
				// Hide all
				$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
				// Show the hidden items that are equal to assigned value
				$(currentInstance).find('[data-show="' + type + '@' + selectedNow + '"]').removeClass('x-hide');

				// If the listener is hidden hide the items
				// ====================
				var controlHidden = $(currentInstance).find('tr[data-listener="' + type + '"]').hasClass('x-hide');

				if( controlHidden ) {

					$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
					$(currentInstance).find('[data-hide^="' + type + '@"]').addClass('x-hide');
				}

				// The event listener for changes
				$(this).change( function() {

					// Get the selected value
					var selected = $(value).find('input[type="text"], input[type="checkbox"]:checked, select').val();

					if( typeof selected !== 'undefined' ) {

						selected.toLowerCase().replace(' ','-');
					}

					// If it's a greater than listener use the text not the value
					if( type.indexOf('^gte') !== -1 ) {

						selected = $(value).find('input[type="text"], input[type="checkbox"]:checked, select option:selected').text();
					}

					// For numeric values check the greater than
					// ====================
					if( $.isNumeric( selected  ) ) {

						// Cleat out the array
						greaterThan = [];

						// Make any array of the greater than values
						$.each( greaterItems, function() {

							// Get just the number
							var theNumber = $(this).data('show').substr( $(this).data('show').indexOf('^gte:') );

							theNumber = theNumber.replace('^gte:','');

							// Make into numbers
							selected = parseInt(selected);
							theNumber = parseInt(theNumber);

							// For the items that are greater than or equal to selected
							if( selected >= theNumber ) {
								// Put in an array
								greaterThan.push( $(this).data('show') );
							}
						});

						// Hide them all
						$(currentInstance).find('[data-show^="' + type + '"]').addClass('x-hide');

						// Show the ones that are greater than or equal to the input
						$.each( greaterThan, function( index ) {
							$(currentInstance).find( '[data-show="' + greaterThan[index] + '"]' ).removeClass('x-hide');
						});
					}

					// Hide the items
					// ====================
					// Show all (reset)
					$(currentInstance).find('[data-hide^="' + type + '@"]').removeClass('x-hide');
					// Hide items that are equal to assigned value
					$(currentInstance).find('[data-hide="' + type + '@' + selected + '"]').addClass('x-hide');

					// Show the items
					// ====================
					// Hide all (reset)
					$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
					// Show the hidden items that are equal to assigned value
					$(currentInstance).find('[data-show="' + type + '@' + selected + '"]').removeClass('x-hide');

				});

			}); // End loop

		} // End instance loop

	}
}






/**
 * Toggles content on click
 */

EasyEdit.plugins.initTemplateMetadata = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initMetadataClick);

	},// End init

	initMetadataClick: function() {

		var toggleWrapper = $('.form-toggle');

		$.each(toggleWrapper, function(key, value) {

			var toggleButton	= $( $(value).find('.form-toggle-btn') );
			var tableWrapper	= $( $(value).find('.form-toggle-body') );
			var sectionHeading	= $( $(value).find('.section-heading') );

			sectionHeading.click(function() {

				tableWrapper.slideToggle();

				$(this).toggleClass('open');

				if (toggleButton.val() == 'Show Options') {

					toggleButton.val('Hide Options');

				} else {

					toggleButton.val('Show Options');
				}

			});
		});
	}
}
/* ################################################################################################
#
#    DATA MANAGEMENT FUNCTIONS
#    
################################################################################################ */

function sqPersAudienceTextToObj (audienceText){
    if (typeof(audienceText) === "undefined"){
        return {};
    }
    if (audienceText === null){
        return {};
    }
    if (audienceText.length < 2){
        return {};
    }
    try {
        audienceObj = JSON.parse(audienceText);
        return audienceObj;
    } catch (e) {
        console.log("Error parsing audience json from metadata text field");
        return false;
    }
}

function sqPersAudienceObjToJSON (obj){
    // to do: normalise obj / json!
    return JSON.stringify(obj);
}

function sqPersAddRestrictionToAudience(audience, restriction){
    // to do: add options for setting restrictions
    if (_.isUndefined(restriction)){
        audience.restrict = "Must";
    }else{
        audience.restrict = restriction;
    }
    return audience;
}
function sqPersAddWeightToAudience(audience, weighting){
    weighting = weighting + 0;
    audience.weight = weighting;
    return audience;
}

function sqPersAddTraitValueToAudience (audience, trait, value){
    var remove = true;
    //to do: normalise value!
    if (typeof(audience['attr']) == "undefined"){
        audience['attr'] = {};
    }
    // if there is no value - we are removing the trait
    if (value !== null){
        if (!_.isUndefined(value)){
            if (value.length > 0){
                if (!_.isUndefined(value[0])){
                    if (value[0] !== ""){
                        remove = false;
                    }
                }
            }
        }    
    } 
    if (remove){
        delete audience['attr'][trait];
    } else {
        audience['attr'][trait] = value;
    }
    return audience;
}

function sqPersAddTraitValueToModel (model, trait, value){
    //to do: normalise value!
    // make sure we dont have an null model
    if (_.isNull(model)){
        model = {};
    }
    // in case there is no traits field - add one
    if (typeof(model['traits']) == "undefined"){
        model['traits'] = {};
    }
    // if the value is blank (or blank array) remove it from the model instead of keeping an empty field
    if (!Array.isArray(value) || !value.length){
        delete model.traits[trait];
    }else{
        model['traits'][trait] = value;
    }
    return model;
}


function sqPersGetSchema(){
    return schema;
}

function sqPersGetFBSettings(){
    return SQ_PERSONALISE_G.FUNNELBACK;
}
/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */

/*
    schema = {
        "key": {
            name: "Friendly name",
            key: "key",
            type: "select" | "text",
            subtype: "multi" | "boolean" | "number",
            note: "Helpful note for editing and testing tools",
            example: "Helpful example for editing tool",
            showInTest: "Y" | "N",
            options: { "key": "Value", "key2": "Value 2"} // only for selects
        }
    }

*/

var schema, presets;
if (_.isUndefined(SQ_PERSONALISE_G)){
    SQ_PERSONALISE_G = {};   
}
schema = _.get(SQ_PERSONALISE_G, "DATA.SCHEMA");
presets = _.get(SQ_PERSONALISE_G, "DATA.PRESETS");

/* ################################################################################################
#
#    AUDIENCE EDITOR FUNCTIONS
#    
################################################################################################ */

EasyEdit.plugins.SqPersAudienceEditor = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', initSqPersAudienceEditor);
		$('body').on('change', '.sq-pers-trait-select', changeTraitSelect);
		$('body').on('change', '.sq-pers-add-trait-value', changeTraitValue);
		$('body').on('click', '.sq-pers-save-new-trait-matching-rule', saveNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-add-audience-button', addNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-cancel-new-trait-matching-rule', cancelNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-audience-preview__edit', editExistingTraitMatchingRule);
		$('body').on('change', '.sq-pers-audience-weighting', updateWeightingSliderOutput);
		$('body').on('change', '.sq-pers-audience-preview__logic-list', updateRestriction);
        $('body').on('click', '.sq-pers__edit-target', showPersonaSelector);
        $('body').on('click', '.sq-pers__modal .closeModal', sqPersCloseModal);
        $('body').on('click', '.sq-pers__persona-list__persona', sqPersEditSwitchPersona);
        $('body').on('click', '#sq-pers__save-persona', sqPersEditSavePersona);
	},// End init
}

function initSqPersAudienceEditor(){
    
    // add the button to any container templates
    // for each container
    targetTemplate = _.template($('#sq-pers-edit-target-button').html());
    schema = sqPersGetSchema();
    assetID = $('.ees-tab-screen__tab').data('id');
    $('.bodycopy_tools.editable').each(function(key,val){
        // if set to block level
            // TODO: check if block level
            // get the metadata of the asset
            // get position
            var position = -1;
            _.each($(val).closest('.bodycopy_manager_div').attr('class').split(' '), function(classVal,classKey){
                if (classVal.indexOf('position-') === 0){
                    if (typeof(classVal) !== "undefined"){
                        position = classVal.split('-')[1];
                    }
                }
            });
            // get asset ID
            if (position > 0){
                var bodyCopy = EasyEditScreens.screen.contentPageStandard.bodycopyManagers[assetID].manager.getDivByPosition(position);
                var bodyCopyID = bodyCopy.id;
                if (typeof(bodyCopyID) !== "undefined"){
                    EasyEditAssetManager.getMetadata({id: bodyCopyID}, function(meta){ 
                        var obj, personaData;
                        console.log(meta); 
                        obj = {};
                        obj.assetid = bodyCopyID;
                        // if the metadata has personalisation settings set - add them to the object
                        if (typeof(meta) !== "undefined"){
                            if (typeof(meta["target.personas"]) !== "undefined"){
                                personaData = JSON.parse(meta["target.personas"]);
                                if (typeof(personaData[0]) !== "undefined"){
                                    obj.personacode = personaData[0];
                                    obj.persona = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {code: obj.personacode});
                                }
                            }
                        }
                        if (!obj.persona){
                            obj.persona = null;
                        }
                        // get the template
                        output = targetTemplate(obj);
                        // render the button
                        $(val).find('.bodycopy_properties.icon').after(output);
                    });            
                }
            }
        // else
            // add button with warning
    });

    // for container templates
    if( $('.sq-pers-audience-editor').length > 0 ) {
		// For each instance of the switch ...
		$.each( $('.sq-pers-audience-editor'), function( key, instance ) {
		    drawInitialAudienceEditor(instance, sqPersGetSchema());
		});
    }
    
    // for asset level metadata
    if ($('#ees_editMetadata').length > 0){
        if (!_.isUndefined(SQ_PERSONALISE_G.DATA.FIELD)){
            sqPersAudField = $('#metadata_field_text_' + SQ_PERSONALISE_G.DATA.FIELD + '_value');
            if (sqPersAudField.length > 0){
                instance = $(sqPersAudField).closest('.cellData');
                instance.addClass('sq-pers-audience-editor');
                drawInitialAudienceEditor(instance, sqPersGetSchema());
            }
        }
    }
}

// Render the over all audience editor interface for an individual section
function drawInitialAudienceEditor(instance, schema){
    var $wrapper, field, audienceText, audience;
    console.log('audience editor');
    field = $(instance).children('.sq-form-field');
    field.hide();
    // $wrapper = $(instance).find('.sq-metadata-contents-wrapper');
    // $wrapper.hide();
    
    // current rules:
    audienceText = field.val();
    audience = sqPersAudienceTextToObj(audienceText);
    audience = sqPersPurgeMissingFields(audience, schema);

    // generate current rules 
    template = _.template($('#sq-pers-edit-audience').html());
    output = template({"audience": audience, "schema": schema});
    $(instance).find('.sq-pers-audience-editor-details').remove();
    $(instance).append(output);
}

function showPersonaSelector(e){
    assetid = $(e.currentTarget).closest('.sq-pers__edit-target').data('assetid');
    modal = new EasyEditModal("Assign persona", "Select the persona to target this content container to.",'sq-pers__modal__persona-selector','sq-pers__modal');

    // generate current rules 
    template = _.template($('#sq-pers-persona-selector').html());
    current = "international";
    output = template({"personas": SQ_PERSONALISE_G.DATA.PRESETS, "current": current, "assetid": assetid});

    modal.addContent(output);


    html = modal.getTemplate();
    $('body').remove('#sq-pers__modal__persona-selector');

    // Remove any old overlay information
    $('#ees_assetFinderOverlay').remove();
    $('body').prepend('<div id="ees_assetFinderOverlay"></div>');
    var $container = jQuery('#ees_assetFinderOverlay');
    $container.html(html);

}
function sqPersCloseModal(e){
    $('.sq-pers__modal').remove();
    $('#ees_assetFinderOverlay').remove();
}
function sqPersEditSwitchPersona(e){
    $('.sq-pers__persona-list__persona').removeClass('selected');
    $(e.currentTarget).addClass('selected');
}
function sqPersEditSavePersona(e){
    //set the relevant metadata
    currentPersona = $('.sq-pers__persona-list__persona.selected').data('code');
    sqpersJSAPI = new EasyEditAPI();
    assetid = $('.sq-pers__persona-list').data('assetid');
    fieldid = "1993";
    $('#ees_assetFinderOverlay').remove();
    EasyEditOverlay.showOverlay('loading');
    sqpersJSAPI.JSAPI.setMetadata({
       "asset_id": parseInt(assetid),
       "field_id": parseInt(fieldid),
       "field_val": '["' + currentPersona + '"]',
    }).done(function(data){
        console.log(data);
        // change the display in the body copy
        obj = {};
        obj.assetid = assetid;
        obj.personacode = currentPersona;
        obj.persona = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {code: obj.personacode});
        targetTemplate = _.template($('#sq-pers-edit-target-button').html());
        output = targetTemplate(obj);
        $marker = $('.sq-pers__edit-target--' + assetid).prev('.bodycopy_properties');
        $('.sq-pers__edit-target--' + assetid).remove();
        $marker.after(output);
        EasyEditOverlay.hideOverlay('loading');
        // cancel the modal            
    });
}


function sqPersPurgeMissingFields(audience, schema){
    for (key in audience.attr){
        if (typeof(schema[key]) === "undefined"){
            delete audience.attr[key];
        }
    }
    return audience;
}

function audienceSchemaFieldToFormRow(schema, field, $target, options){
    var output, audience, audienceText, data;
    
    // get the audience
    audienceText = $target.closest('.sq-pers-audience-editor').children('.sq-form-field:not([name$=_default])').val();
    audience = sqPersAudienceTextToObj(audienceText);
    
    data = {
        "schema": schema, 
        "field": field, 
        "audience": audience
    };
    if (typeof(options) !== "undefined"){
        if (options.edit){
            data.edit = true;
        }else{
            data.edit = false;
        }
    }
    
    template =  _.template($('#sq-pers-edit-audience-matching').html());
    output = template(data);
    $target.html(output);
}


// 
function saveNewTraitMatchingRule(e){
    var value, audience, trait, $field, wrapper, fieldType, restriction, weighting;

    // serialise changes and update JSON field 
    $container = $(e.currentTarget).closest('.audience-matching-rules-inner').find('.sq-pers-add-trait-value');
    
    // get the top level wrapper
    wrapper = $container.closest('.sq-pers-audience-editor');
    
    // get the trait
    trait = $container.data('key');
    
    // get the form field
    $field = wrapper.children('.sq-form-field:not([name$=_default])');
    
    // get the current audience
    audienceText = $field.val();
    audience = sqPersAudienceTextToObj(audienceText);
    
    // get the restriction type
    restriction = wrapper.find('.sq-pers-audience-preview__logic-list').val();
    
    // get the weighting
    weighting = parseInt(wrapper.find('.sq-pers-audience-weighting').val());
    
    // different processing for different field types
    fieldType = $container.data('fieldType');
    if (fieldType === "select-multi"){
        val = $container.find('input.sq-pers-trait-checkbox[type=checkbox]:checked').map(function(_, el) {
            return $(el).val();
        }).get();
        value = val;    
    }else if (fieldType === "select-select"){
        value = [$container.find('select').val()];
    }else{
        value = [$container.find('input').val()];
    }
    
    // save value to audience
    audience = sqPersAddTraitValueToAudience(audience, trait, value);
    audience = sqPersAddRestrictionToAudience(audience, restriction);
    audience = sqPersAddWeightToAudience(audience, weighting);
    json = sqPersAudienceObjToJSON(audience);
    
    // in case we are in metadata screen mode, trigger a click on the use default checkbox if it is checked
    $container.closest('.sq-pers-audience-editor').find('input[name$=_default][checked=checked]').trigger('click');
    // enable the save button
    $('#ees_saveButtonAction').removeClass('disabled');    
    
    // save audience to text field
    $field.val(json);
    
    // remove the interface
    // $(e.currentTarget).closest('.audience-matching-rules-inner').remove();
    drawInitialAudienceEditor(wrapper, sqPersGetSchema());
}

function updateWeightingSliderOutput(e){
    $(e.currentTarget).parent().find('.weighting-output').text($(e.currentTarget).val());
    // if value set - active add button
    if ($(e.currentTarget).closest('.sq-pers-edit-add-row').find('.sq-pers-add-trait-value input:checked, .sq-pers-add-trait-value select:selected, .sq-pers-add-trait-value input[type=text]').val().length > 0){
        changeTraitValue();
    }
}
function updateRestriction(e){
    // if value set - activate add button   
    if ($(e.currentTarget).closest('.sq-pers-edit-add-row').find('.sq-pers-add-trait-value input:checked, .sq-pers-add-trait-value select:selected, .sq-pers-add-trait-value input[type=text]').val().length > 0){
        changeTraitValue();
    }
}
// cancel creation of a new audience matching rule
function cancelNewTraitMatchingRule(e){
    $(e.currentTarget).closest('.audience-matching-rules-inner').remove();
}

// Show the add button after the user has selected a value to match on the given trait
function changeTraitValue(e){
    // show the add button
    $('.sq-pers-save-new-trait-matching-rule').show();
}

// Re render the audience template after the user has selected a trait to match on
function changeTraitSelect(e){
    var val;
    val = $(e.currentTarget).val();
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');    
    audienceSchemaFieldToFormRow(sqPersGetSchema(), val, $target, {});
}

// Add a new rule interface to configure an individual rule, click on "add audience matching rule" button
function addNewTraitMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');
    audienceSchemaFieldToFormRow(sqPersGetSchema(), "", $target, {});
}

// Open the rule interface to configure an individual rule, with existingvalues
function editExistingTraitMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');
    field = $(e.currentTarget).data('traitKey');
    audienceSchemaFieldToFormRow(sqPersGetSchema(), field, $target, {edit: true});
}



/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

EasyEdit.plugins.ImageRelatedAssetViewer = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initSqPersEditColorInput);
	},// End init

	// Hide what's required and listen for changes
	initSqPersEditColorInput: function() {
	    console.log('color plugin');
	    $('.sq-pers-edit-color-input').find('input').each(function(key,val){
            $(val).attr('type', 'color');
	    });
	}
}

/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

console.log('test 1');
EasyEdit.plugins.SqPersImageRelatedAssetViewer = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
	    console.log('test 2');
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initImageRelatedAssetViewer);
		$('body').on('change', 'sq-image-related-asset-viewer input[name$="_assetid"]', testO)

	},// End init

	// Hide what's required and listen for changes
	initImageRelatedAssetViewer: function() {
		if( $('.sq-image-related-asset-viewer').length > 0 ) {

			// For each instance of the switch ...
			$.each( $('.sq-image-related-asset-viewer'), function( key, instance ) {
			      console.log('found sq-image-related-asset-viewer');
			      
			      assetid = $(instance).find('input[name$="_assetid"]').val();
			      target = $(instance).find('.sq-image-related-asset-viewer__img');
			      sqImageRelatedAssetViewerUpdate(assetid, target);
			      
			      $(instance).find('input[name$="_assetid"]').on('change', function(e){
			          assetid = $(e.currentTarget).val();
			          sqImageRelatedAssetViewerUpdate(assetid, $(e.currentTarget).closest('.sq-image-related-asset-viewer').find('.sq-image-related-asset-viewer__img'))
			      });
			});
	    
		}
	}
}

function testO(e){
    console.log(TESTO);
}

function sqImageRelatedAssetViewerUpdate(assetid, target){
    var baseurl, url;
    baseurl = window.location.href.replace(/_edit.*/, '');
    url = baseurl + '?a=' + assetid;
    if (assetid > 0){
        $(target).css('background-image', 'url("' + url + '")');
        $(target).removeClass('screenHide');
    }else{
        $(target).addClass('screenHide');
    }
    console.log('TEST2');
}

/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */
var schema, presets;
if (_.isUndefined(SQ_PERSONALISE_G)){
    SQ_PERSONALISE_G = {};   
}
schema = _.get(SQ_PERSONALISE_G, "DATA.SCHEMA");
presets = _.get(SQ_PERSONALISE_G, "DATA.PRESETS");

/* ################################################################################################
#
#    JSON LIST EDITOR PLUGIN (user data schema, personas, tracking rules)
#    
################################################################################################ */

EasyEdit.plugins.SqPersJsonListEditor = {

    /**
     * Initialise the plugin
     */
    init: function()
    {
        var self = this;
        // Add a function to the after ees load event
        EasyEditEventManager.bind('EasyEditScreenLoad', SqPersJLE.jsonListEditor);
        $('body').on('click', '.sq-pers-json-list-editor__display .section-heading', SqPersJLE.toggleItem);
        $('body').on('change', '.sq-pers-json-list-editor__display [data-listen] input, .sq-pers-json-list-editor__display [data-listen] select', SqPersJLE.listenerChanged);
        $('body').on('click', '.sq-pers-json-add-row', SqPersJLE.addRow);
        $('body').on('change', '.sq-pers-json-keyval', SqPersJLE.keyvalChange);
        $('body').on('click', '.sq-pers-json-keyval__remove', SqPersJLE.keyvalRemove);
        $('body').on('change', '.sq-pers-json__val', SqPersJLE.enableUpdateButton);
        $('body').on('click', '.sq-pers-save-row', SqPersJLE.saveRow);
        $('body').on('click', '.sq-pers-cancel-row', SqPersJLE.cancelRow);
        /*
        $('body').on('change', '.sq-pers-trait-select', changeTraitSelect);
        $('body').on('change', '.sq-pers-add-trait-value', changeTraitValue);
        $('body').on('click', '.sq-pers-save-new-trait-matching-rule', saveNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-add-audience-button', addNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-cancel-new-trait-matching-rule', cancelNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-audience-preview__edit', editExistingTraitMatchingRule);
        $('body').on('change', '.sq-pers-audience-weighting', updateWeightingSliderOutput);
        $('body').on('change', '.sq-pers-audience-preview__logic-list', updateRestriction);
        */
    },// End init
    
}
// SqPersJsonListEditor
var SqPersJLE = {
    jsonListEditor: function(e){
        var self = this;
        // for each editor
        if( $('.sq-pers-json-list-editor').length > 0 ) {
            // For each instance of the editor
            $.each( $('.sq-pers-json-list-editor'), function( key, instance ) {
                $(instance).find('.sq-pers-json-list-editor__field .sq-form-field').hide();
                SqPersJLE.drawJsonListEditor(instance, null, false);
            });
        }
    },
    drawJsonListEditor: function(instance, active, add){
        var schema, data, namefield, categoryfield;
        console.log("drawJsonListEditor");
        
        // get data and schema to figure out what to draw
        data = $(instance).find('.sq-form-field').val();
        schema = $(instance).next('.sq-pers-json-list-editor__schema').html();
        categoryfield = $(instance).data('categoryfield');
        namefield = $(instance).data('namefield');
        keyfield = $(instance).data('keyfield');
        type = $(instance).data('type');
        
        if(_.isUndefined(data) || _.isUndefined(schema)){
            return false;
        }
        
        if (data !== ""){

            data = JSON.parse(data);
            schema = JSON.parse(schema);

            // if adding a new row
            if (add){
                if (type === "object"){
                    data.NEW_ROW = {};
                    active = "NEW_ROW";
                }else{
                    data.push({});
                    active = data.length;
                }
            }
            
            // for each row, draw an accordion - pass through the data, the schema and whether a particular row is active
            template = _.template($('#sq-pers-json-list-editor__template').html());
            templateData = {
                data: data, 
                schema: schema, 
                active: active, 
            }
            if (!_.isUndefined(namefield)){
                templateData.namefield = namefield;
            }
            if (!_.isUndefined(namefield)){
                templateData.categoryfield = categoryfield;
            }
            if (!_.isUndefined(keyfield)){
                templateData.keyfield = keyfield;
            }
            output = template(templateData);
            $(instance).find('.sq-pers-json-list-editor__display').remove();
            $(instance).append(output);
            
            // handle show if
            SqPersJLE.handleShowIf(instance);
            
            // focus if a section is open
            $(instance).find('.form-toggle-body:visible input').first().focus();
            
            // broadcast event that json list editor is drawn
            //$(instance).trigger("json-list-editor-rendered");
            $(instance).trigger("json-list-editor-rendered");
        }            
    },  
    toggleItem: function(e){
        value = $(e.currentTarget).closest('.form-toggle');
        var toggleButton = $($(value).find('.form-toggle-btn'));
        var tableWrapper = $($(value).find('.form-toggle-body'));
        var sectionHeading = $($(value).find('.section-heading'));
        var updateButton = $(tableWrapper).find('.sq-pers-save-row');
        if (updateButton.is(':not([disabled])')){
            confirmed = false;
            conf = confirm('If you continue you will lose changes in the currently expanded section. Do you want to continue?');
            if (conf){
                confirmed = true;
            }
        }else{
            confirmed = true;
        }

        if (confirmed){
            up = false;
            if (toggleButton.val() == '▼'){
                up = true;
            }
            
            // hide the other items
            list = $(e.currentTarget).closest('.sq-pers-json-list-editor__display');
            $(list).find('.section-heading').removeClass('open');
            $(list).find('.form-toggle-body').slideUp();
            $(list).find('.form-toggle-btn').val('▼');
            
            if (up){
                tableWrapper.slideDown();
                sectionHeading.addClass('open');
                if (toggleButton.val() == '▼') {
                    toggleButton.val('▲');
                } else {
                    toggleButton.val('▼');
                }
            }
        }
    },
    handleShowIf: function(instance){
        // for each show - hide it
        $(instance).find('[data-show]').hide();
        $(instance).find('[data-show]').each(function(key,showIfInstance){
            // which section is the field in?
            section = $(showIfInstance).closest('.form-toggle-body');
            // which field is it listening to?
            listenTo = $(showIfInstance).data('show');
            // if we have a value it is listening to...
            if (!_.isUndefined(listenTo)){
                // split value to get field and value
                listenTo = listenTo.split('@');
                // find the appropriate listener within this section
                listener = $(section).find('[data-listen^=' + listenTo[0] + '] .sq-pers-json__val');
                // check if the listener's current value matches the value
                if ($(listener).val() === listenTo[1]){
                    $(showIfInstance).show();
                }
            }
        });
    },
    listenerChanged: function(e){
        // item has changed
        // check for anything listening to this
        container = $(e.currentTarget).closest('.sq-pers-json-list-editor__display');
        listenVal = $(e.target).closest('.form-group').data('listen');
        target = $(container).find('[data-show^=' + listenVal + ']');
        $(target).toggle();
    },
    addRow: function(e){
        instance = $(e.currentTarget).closest('.sq-pers-json-list-editor')[0];
        SqPersJLE.drawJsonListEditor(instance, null, true);
    },
    keyvalChange: function(e){
        // if this row is an add row
        row = $(e.currentTarget).closest('.sq-pers-json-keyval');
        if(row.hasClass('sq-pers-json-keyval__add')){
            // if both key and value are changed
            if (row.find('.sq-pers-json__val').val().length > 0 && row.find('.sq-pers-json__val__value').val().length > 0 ){
                // remove add class
                row.removeClass('sq-pers-json-keyval__add');
                // get fieldname
                fieldname = row.find('.sq-pers-json__val').attr('name').split('_')[0];
                // add a new blank row
                row.after(  '<div class="sq-pers-json-keyval sq-pers-json-keyval__add">' +
                            '   <input  class="sq-pers-json__val"' +
                            '           type="text"' +
                            '           value=""' +
                            '           name="' + fieldname + '_key"' +
                            '   />' +
                            '   <input  class="sq-pers-json__val__value"' +
                            '           type="text"' +
                            '           value=""' +
                            '           name="' + fieldname + '_value"' +
                            '   />' +
                            '   <a href="javascript:void(0)">Remove</a>' +
                            '</div>');
                // focus on the new field
                row.next().find('.sq-pers-json__val').focus();
            }
        }
    },
    keyvalRemove: function(e){
        target = $(e.currentTarget).closest('.sq-pers-json-keyval');
        $(target).slideUp(function(){
            $(target).remove();
        });
        SqPersJLE.enableUpdateButton(e);
    },
    enableUpdateButton: function(e){
        $(e.currentTarget).closest('.form-toggle-body').find('.sq-pers-save-row').prop('disabled', false);
    },
    saveRow: function(e){
        wrapper = $(e.currentTarget).closest('.sq-pers-json-list-editor');
        wrapperKeyfield = $(wrapper).data('keyfield');
        row = $(e.currentTarget).closest('.form-toggle-body');
        field = wrapper.find('.sq-pers-json-list-editor__field textarea'); 
        rowkey = $(row).data('key');
        
        // get source data as JSON
        fieldData = field.val();
        data = JSON.parse(fieldData);
        
        rowData = {};
        //if (wrapper.type === "object"){
          
        // recompile the data from the current open row
        row.find('.sq-pers-json-form-group').each(function(key, fieldGroup){
            type = $(fieldGroup).data('type'); 
            key = $(fieldGroup).data('key');
            iskey = $(fieldGroup).data('iskeyfield');
            origvalue = $(fieldGroup).data('origvalue');
            if (iskey == 1){
                rowData.origvalue = origvalue;
            }
            if (type === "text" || type === "color" || type === "json"){
                rowData[key] = $(fieldGroup).find('.sq-pers-json__val').val();
            }else if (type === "select"){
                rowData[key] = $(fieldGroup).find('.sq-pers-json__val').val();
            }else if (type === "keyval"){
                keyVals = {};
                $(fieldGroup).find('.sq-pers-json-keyval').each(function(key,val){
                   inputKey = $(val).find('.sq-pers-json__val').val();
                   inputVal = $(val).find('.sq-pers-json__val__value').val();
                   if (inputKey && inputVal){
                        keyVals[inputKey] = inputVal; 
                   }
                });
                rowData[key] = keyVals;
            }
        });
        
        // Merge the new rows with the data - handling special cases
        
        // if this is an object - as opposed to an array
        if (wrapper.data('type') === "object"){
            // Special case if this is the NEW_FIELD
            if (rowkey === "NEW_FIELD"){
                rowkey = rowData[wrapperKeyfield];
            }
            // Special case if the key changes!!
            if (rowData[wrapperKeyfield] !== rowkey){
                // remove the element at rowkey
                delete(data[rowkey]);
                // set rowkey to rowData[wrapperKeyField]
                rowkey = rowData[wrapperKeyfield];
            }
            
            // update the data object
            data[rowkey] = rowData;
        // this is an array - so loop over it and find the right row to replace
        }else{
            // TODO!
            // for each item in the array
            _.each(data, function(arrayRow,key){
                
                // special case if this is the NEW_FIELD
                if (rowData[wrapperKeyfield] === "NEW_FIELD") {
                    // do nothing becase it just gets set anyway?
                }
                
                // if the key field was updated - we will see 'origvalue'
                if (typeof(rowData.origvalue) !== "undefined"){
                    // if the current data row's key matches the edited row's original key - this is the correct row
                    if (rowData.origvalue === arrayRow[wrapperKeyfield]){
                        // has the key changed?
                        delete(rowData.origvalue);
                        data[key] = rowData;
                    }
                    // TODO - check for clash, e.g. unrelated row is given the same key
                }

            });
        }
        
        // put source data into field
        field.val(JSON.stringify(data));
        
        // enable save button
        $('#ees_saveButtonAction').removeClass('disabled');
        
        // re draw
        SqPersJLE.drawJsonListEditor(wrapper, null, false);
        
    },
    cancelRow: function(e){
        // if this is a new row - remove it
        /*
        instance = $(e.currentTarget).closest('.form-toggle');
        if ($(instance).find('.sq-pers-json__new-row').length > 0){
            
        }*/
        // otherwise: just  redraw
        instance = $(e.currentTarget).closest('.sq-pers-json-list-editor');
        SqPersJLE.drawJsonListEditor(instance, null, false);
    }
}

$(document).ready(function(e){
    $('body').on('json-list-editor-rendered', function(e){
        console.log('json-list-editor-rendered')
        console.log(e); 
    });
});
SqPersJLE.customTraitFunction = function(e){
    // load the schema
    // create an interface for selecting traits
    // every time traits are selected - render down to text field
    
}
/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */

/* ################################################################################################
#
#    Query Builder functions
#    
################################################################################################ */
//SQ_PERSONALISE_G.FUNNELBACK.FIELDS

EasyEdit.plugins.SqPersQueryBuilder = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', initSqPersQueryBuilder);
		$('body').on('click', '.sq-pers-add-query-button', addNewQueryMatchingRule)
		$('body').on('change', '.sq-pers-querytrait-select', changeQueryTraitSelect);
		$('body').on('change', '.sq-pers-edit-add-row__queryfixed', fixedQueryEntered);
		$('body').on('click', '.query-builder-rules .sq-pers-save', saveQueryRow);
		$('body').on('click', '.sq-pers-query-editor-preview__delete', deleteQueryRow);
	},// End init
}

function initSqPersQueryBuilder(){
    if( $('.sq-pers-query-builder').length > 0 ) {
		// For each instance of the switch ...
		$.each( $('.sq-pers-query-builder'), function( key, instance ) {
		    drawInitialQueryBuilder(instance, sqPersGetSchema(), sqPersGetFBSettings());
		});
    }
}

// Render the over all audience editor interface for an individual section
function drawInitialQueryBuilder(instance, schema, fbsettings){
    console.log('query builder');
    var $wrapper, audienceText, audience;
    //$wrapper = $(instance).find('.sq-metadata-contents-wrapper');
    $field = $(instance).children('.sq-form-field');
    $field.hide();
    // $wrapper.hide();
    
    // current rules:
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    friendlyQueries = sqPersQueriesToFriendlyQueries(queries, schema, fbsettings);

    // generate current rules 
    template = _.template($('#sq-pers-query-builder').html());
    output = template({"queries": friendlyQueries, "schema": schema, "fbsettings": fbsettings});
    $(instance).find('.sq-pers-editor-details').remove();
    $(instance).append(output);
}


// Add a new rule interface to configure an individual rule, click on "add audience matching rule" button
function addNewQueryMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-query-builder');
    $target = $container.find('.query-builder-rules');
    renderQueryMatchingRuleForm(sqPersGetSchema(), "", $target, {}, sqPersGetFBSettings());
}

function renderQueryMatchingRuleForm(schema, field, $target, options, fbsettings){
    //var output, audience, audienceText, data;
    
    // get the audience
    //audienceText = $target.closest('.sq-metadata-wrapper').find('.sq-form-field').val();
    //audience = sqPersAudienceTextToObj(audienceText);
    
    data = {
        "schema": schema, 
        "field": field, 
        "fbsettings": fbsettings
    };
    if (typeof(options) !== "undefined"){
        if (options.edit){
            data.edit = true;
        }else{
            data.edit = false;
        }
    }
    
    template =  _.template($('#sq-pers-edit-query-builder').html());
    output = template(data);
    $target.html(output);
}

// Re render the audience template after the user has selected a trait to match on
function changeQueryTraitSelect(e){
    var val;
    val = $(e.currentTarget).val();
    $container = $(e.currentTarget).closest('.sq-pers-editor-details');
    $target = $container.find('.query-matching-rules');    
    renderQueryMatchingRuleForm(sqPersGetSchema(), val, $target, {}, sqPersGetFBSettings());
}

function fixedQueryEntered(e){
    if ($(e.currentTarget).val().length > 0){
        $(e.currentTarget).closest('.form-group').find('.sq-pers-querytrait-select').attr('disabled', 'disabled');
    }else{
        $(e.currentTarget).closest('.form-group').find('.sq-pers-querytrait-select').removeAttr('disabled');
    }
}

function saveQueryRow(e){

    var trait, fixed, fbfield, queries, fbquery, $container, $field, instance;

    $container = $(e.currentTarget).closest('.sq-pers-edit-add-row__inner');
    
    instance = $container.closest('.sq-pers-query-builder');
    
    $field = instance.find('.sq-form-field');

    // get the trait
    trait = $container.find('.sq-pers-querytrait-select:enabled').val();
    
    // if undefined - then get the fixed value
    if (_.isUndefined(trait)){
        fixed = $container.find('.sq-pers-edit-add-row__queryfixed').val();
        if (_.isUndefined(fixed)){
            // can't save - error
            alert('Please select a trait or enter a fixed value.');
            return false;
        }else{
            trait = fixed;
        }
    } else {
        // wrap the trait in markers if it is from an audience trait - leave plain for hard coded
        trait = "{{" + trait + "}}";
    }
    
    fbfield = $container.find('.sq-pers-queryfield-select').val()
    if (_.isUndefined(fbfield)){
        alert('Please select a query field.');
        return false;
    }
    
    // get the queries
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    
    // save value to audience
    queries = sqPersAddQueryValueToQueries(queries, trait, fbfield);
    queryURL = sqPersQueriesObjToURL(queries);
    
    // save audience to text field
    $field.val(queryURL);
    
    // remove the interface
    drawInitialQueryBuilder(instance, sqPersGetSchema(), sqPersGetFBSettings());
}

function deleteQueryRow(e){
    var fbfield, queries, $instance, queryText, queryURL;
    
    // get the container for this instance of the query builder
    $instance = $(e.currentTarget).closest('.sq-pers-query-builder');
    
    // get the funnelback field name
    fbfield = $(e.currentTarget).closest('.sq-pers-current-state-preview__row').data('fbfield');
    
    // get the queries
    $field = $instance.find('.sq-form-field');
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    
    // update the queries
    queries = sqPersAddQueryValueToQueries(queries, "", fbfield);
    queryURL = sqPersQueriesObjToURL(queries);
    
    // save audience to text field
    $field.val(queryURL);
    
    // remove the interface
    drawInitialQueryBuilder($instance, sqPersGetSchema(), sqPersGetFBSettings());
}

function sqPersQueryTextToObj(queryText){
    if (_.isUndefined(queryText)){
        return [];
    }
    if (queryText.length < 1){
        return [];
    }
    texts = queryText.split('?')[1].split('&');
    pairs = [];
    _.each(texts, function(text, key){
        pair = text.split('=');
        if (pair[0] !== "profile" && pair[0] !== "collection" && pair[0] !== "query"){
            pairs.push(pair);
        }
    });
    return pairs;
}
function sqPersAddQueryValueToQueries(queries, trait, fbfield){
    var index;
    if (_.isUndefined(queries)){
        queries = [];
    }
    index = -1;
    _.each(queries, function(val,key){
        if (val[0] === fbfield){
            index = key;
        }
    });
    // if the field already exists in the array - update it - otherwise add a new row
    if (index > -1){
        // if the trait is specified - add or set it - otherwise remove it
        if (trait.length > 0){
            queries[index] = [fbfield, trait]    
        }else{
            queries.splice(index, 1);
        }
    }else{
        // only add the trait if it is specified
        if (trait.length > 0){
            queries.push([fbfield, trait]);
        }
    }
    return queries;
}
function sqPersQueriesObjToURL(queries){
    var fburl;
    //fburl = SQ_PERSONALISE_G.FUNNELBACK.DOMAIN + 
    //"?collection=" + SQ_PERSONALISE_G.FUNNELBACK.COLLECTION +
    //"&profile=" + SQ_PERSONALISE_G.FUNNELBACK.PROFILE +
    fburl = "{{ fbdomain }}?collection={{ fbcollection }}&profile={{ fbprofile }}&query=!null"; 
    _.each(queries, function(val,key){
       fburl = fburl + "&" + val[0] + "=" + val[1]; 
    });
    return fburl;
}

function sqPersQueriesToFriendlyQueries(queries, schema, fbsettings){
    var friendlyQueries = [];
    // for each query the user has created
    _.each(queries, function(query, key){
        // for each funnelback field in the schema, find the one that matches the query
       _.each(fbsettings.FIELDS, function(field, key2){
           // if the schema record matches the current query
            if (field.key == query[0]) {
                // if the trait is directly entered by the user
                if (_.isNull(query[1].match("{{"))){
                    field.trait = {"name": query[1], "key": query[1], "hardcoded": true};   
                }else{
                    field.trait = schema[query[1].replace('{{','').replace('}}','')];
                    field.trait.hardcoded = false;
                }
                friendlyQueries.push(field);
            }
       });
    });
    return friendlyQueries;
}
/**
 * Adds a personalisation testing tool to the preview screen in Edit+
 * Expects ... 
 * 
 */


/* ################################################################################################
#
#    PERSONALISATION TESTING TOOL
#    
################################################################################################ */
SQ_PERS_TESTINGTOOL_USER_MODEL = {};
SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = {};
EasyEdit.plugins.SqPersTestingTool = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditAfterLoad', initSqPersTestingTool);
		$('body').on('click', '#ees_sqPersonalise_test__button, .testing-tool-header__close', toggleSqPersTestingTool);
		$('body').on('click', '.sq-pers-test-expand__button', toggleSqPersTestingToolSection);
		$('body').on('change', '.sq-pers-highlight-toggle', toggleHighlighting);
		$('body').on({mouseenter: hoverSqPersPersona, mouseleave: hoverSqPersPersona}, '.sq-pers-preset-link');

		$('body').on('click', '.testing-tool-body .sq-pers-sidebar-tab', toggleSqPersModifiedTraits);
		$('body').on('click', '.sq-pers-preset-link', selectSqPersPersona);
        $('body').on('click', '.persona-header__close', cancelSqPersPersona)
		
		$('body').on('change', '.sq-pers-test-field__input', updateSqPersTestFieldToModel);
		
		
	},// End init
}

function initSqPersTestingTool(e){
    var models;
    console.log('testing tool init');
    if (window.location.hash.match('mode=preview') !== null){
        // data init - get user models from storage
        models = getSqPersModelsFromStorage();
        SQ_PERS_TESTINGTOOL_USER_MODEL = models.userStored;
        SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = models.modifiedStored;
        
        if ($('#ees_sqPersonalise_test__button').length > 0){
        }else{
            
            $('#ees_previewOptions ul').append('<li class="toolbarButton noleft"><a href="javascript:void(0)" id="ees_sqPersonalise_test__button" class="" title="Personalisation testing tool"><span id="ees_sqPersonalise_test__button__name" class="icon"></span></a></li>');
            $('#ees_previewOptions .toolbarButton:not(:last-child)').addClass('noright');
        }
        if ($('#ees_sqpersonalise_test').length > 0){
        }else{
            $('#ees_toolBar').append('<div id="ees_sqpersonalise_test" style=""><h2>Personalisation testing tool</h2></div>');
        }
        console.log("THIS IS PREVIEW MODE");
        redrawSqPersTestingToolData();
    }
}

function toggleSqPersTestingToolSection(e){
    var $target;
    $target = $(e.currentTarget).closest('.sq-pers-test-field').find('.sq-pers-test-expand__section');
    if ($target.is(':visible')){
        // toggle arrow icons
        $('.sq-pers-test-expand__button .up').hide();
        $('.sq-pers-test-expand__button .down').show();
        $(e.currentTarget).find('.down').show();
        // toggle section
        $('.sq-pers-test-expand__section').slideUp();
    }else{
        // toggle arrow icons
        $('.sq-pers-test-expand__button .up').hide();
        $('.sq-pers-test-expand__button .down').show();
        $(e.currentTarget).find('.down').hide();
        $(e.currentTarget).find('.up').show();
        // hide any other open section
        $('.sq-pers-test-expand__section').slideUp();
        // show section
        $target.slideDown(function(){
            $('.sq-pers-test-field.active input').focus();
        });
        
    }
}

function toggleSqPersTestingTool(e){
    $('#ees_sqpersonalise_test').toggleClass('toolvisible');
    setSqPersFieldsFirstLastClasses();
}

function getSqPersModelsFromStorage(){
    // invoke function in front end personalisation code in iframe
    return document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.getSqPersModelsFromStorage();
}
function setSqPersModelsToStorage(models){
    // invoke function in front end personalisation code in iframe
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.setSqPersModelsToStorage(models);
}

// enable highlighting of elements - via front end code in iframe
function toggleHighlighting(e){
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.toggleHighlighting();
}

function updateSqPersTestFieldToModel(e){
    var value, model, trait;
    // get trait from field
    trait = getSqPersTraitKeyFromTestField(e);
    // get values
    value = getSqPersTraitValueFromTestField(e);
    // get model
    model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;
    // update the model
    model = sqPersAddTraitValueToModel(model, trait, value);
    SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = model;
    // serialise the model to localStorage
    setSqPersModelsToStorage({
        userStored: SQ_PERS_TESTINGTOOL_USER_MODEL,
        modifiedStored: SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED
    })
    // redraw the interface
    redrawSqPersTestingToolData();
    
    // repersonalise the preview
    sqPersUpdatePage(model, {});
}
// given a field on change, extract the trait key
function getSqPersTraitKeyFromTestField(e){
    var $target = $(e.currentTarget);
    return $target.data('trait');
}

// given a field on change, extract the new value in the right format
function getSqPersTraitValueFromTestField(e){
    var $target = $(e.currentTarget);
    var result = [];
    if ($target.hasClass('select-multi')){
        _.each($target.find('input:checked'), function(val,key){
            result.push($(val).val()); 
        });
    }else if($target.hasClass('select-select')){
        result.push($target.find('input').val());
    }else if($target.hasClass('text-number')){
        result.push($target.find('input').val());
    }else if($target.hasClass('text-text')){
        result.push($target.find('input').val());
    }
    console.log(result);
    return result;     
}

function redrawSqPersTestingToolData(){
    var schema, model, expanded, presetindex;
    // get and compile the template
    template = _.template($('#sq-pers-testing-tool').html());
    
    // get the schema
    // TODO - don't assume this is here
    schema = SQ_PERSONALISE_G.DATA.SCHEMA;
    presets = SQ_PERSONALISE_G.DATA.PRESETS;
    

    
    // get list of field names to be expanded
    expanded = [];
    _.each($('.sq-pers-test-expand__section:visible .sq-pers-test-field__input'), function(val,key){
       expanded.push($(val).data('trait'));
    });
    
    // get active tab state
    tabstateall = $('.testing-tool-body .sq-pers-sidebar-tab.active').hasClass('sq-pers-all-traits');
    
    // the currently viewed profile/preset
    profile = null;
    profileset = false;
    if ($('.sq-pers-preset-link.enabled').length === 0){
        // check if there is a preset persona code in the model
        model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;
        if (!_.isUndefined(model.code)){
            profile = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {"code": model.code});
            profileset = true;
        }
    }else{
        presetindex = $('.sq-pers-preset-link').index($('.sq-pers-preset-link.enabled'));
        if (presetindex > 0){
            profile = presets[presetindex - 1];
            if (!_.isUndefined(profile.traits)){
                profileset = true;
            }
        } else {
            profile = {"traits": []};
        }            

        // switch the existing model to the profile model
        setSqPersModelsToStorage({
            userStored: SQ_PERS_TESTINGTOOL_USER_MODEL,
            modifiedStored: profile
        });
        // repersonalise the preview
        sqPersUpdatePage(profile, {});
    }

    // merge the audience and the model (getSqPersModelFromTestingTool)
    model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;

    // render the template - with the schema and audience
    output = template({"model": model, "schema": schema, "presets": presets, "expanded": expanded, "tabstateall": tabstateall, "profile": profile, "profileset": profileset});
    
    // set the html
    $('#ees_sqpersonalise_test').html(output);
    
    // setup the initial modified traits tab setting
    toggleSqPersModifiedTraits(undefined);
    
    // if visible, fix the rounded corners for first and last rows
    if ($('#ees_sqpersonalise_test:visible').length > 0){
        setSqPersFieldsFirstLastClasses();
    }
    
    // render boolean switcher fields
    renderBooleanSwitcher();

    // fade in any hidden elements
    $('.sq-pers__fadein').fadeIn(200);

    // make sure we're scrolled to where the user was immediately before redrawing
    $('.sq-pers-test-field.active input').focus();

}

function toggleSqPersModifiedTraits(e){
    console.log('toggle modified traits');
    if (typeof(e) !== "undefined"){
        $(e.currentTarget).parent().find('a').toggleClass('active');
    }
    if ($('.testing-tool-body .sq-pers-sidebar-tab.active').hasClass('sq-pers-all-traits')){
        // show all traits
        $('.sq-pers-test-field').show();
    }else{
        // show only modified traits
        $('.sq-pers-test-field').hide();
        $('.sq-pers-test-field.active').show();
    }
    setSqPersFieldsFirstLastClasses();
}

function setSqPersFieldsFirstLastClasses(){
    // remove then add first and last classes    
    $('.sq-pers-test-fields').each(function(key,val){
        $(val).find('.sq-pers-test-field').removeClass('last first');
        $(val).find('.sq-pers-test-field:visible:last').addClass('last');
        $(val).find('.sq-pers-test-field:visible:first').addClass('first');
    });
}

/* ################################################################################################
#
#    PRESETS
#    
################################################################################################ */

function hoverSqPersPersona(e){
    //$(e.currentTarget).find('.sq-pers-present-link__full').toggle();
    $(e.currentTarget).toggleClass('hovered');
    $(e.currentTarget).closest('.sq-pers-preset-list').toggleClass('active');
}
function selectSqPersPersona(e){
    console.log('selecting persona');
    //$(e.currentTarget).children().toggle().toggleClass('enabled');
    if ($(e.currentTarget).hasClass('enabled')){
        $(e.currentTarget).closest('.sq-pers-preset-list').find('.enabled').removeClass('enabled');
    }else{
        $(e.currentTarget).closest('.sq-pers-preset-list').find('.enabled').removeClass('enabled');
        $(e.currentTarget).addClass('enabled');

        // trigger redraw!
        redrawSqPersTestingToolData();
    }
}
function cancelSqPersPersona(e){
    $('.sq-pers-preset-list').removeClass('enabled');
    $('.sq-pers-preset-link--null').addClass('enabled');
    redrawSqPersTestingToolData();
}

/* ################################################################################################
#
#    Re-render page
#    
################################################################################################ */

function sqPersUpdatePage(userdata, options){
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.filterPersonalisedAreasInDOM(userdata, options);
}

/* ################################################################################################
#
#    Copy of boolean switcher from CCT XP container templates
#    
################################################################################################ */
function renderBooleanSwitcher() {
    if ($('.onoffswitch').length > 0) {
        $.each($('.onoffswitch'), function(key, instance) {
            var onLabel = $(instance).data('on-txt');
            var offLabel = $(instance).data('off-txt');
            $(instance).find('label').empty();
            $(instance).find('label').append('<span data-txt-off="' + offLabel + '" data-txt-on="' + onLabel + '" class="onoffswitch-inner"></span>');
            $(instance).find('label').append('<span class="onoffswitch-switch"></span>');
            var sweetSwitch = $(instance).html();
            sweetSwitch = sweetSwitch.replace('<ul>', '');
            sweetSwitch = sweetSwitch.replace('<li>', '');
            sweetSwitch = sweetSwitch.replace('</li>', '');
            sweetSwitch = sweetSwitch.replace('</ul>', '');
            $(instance).html(sweetSwitch);
        });
    }
}