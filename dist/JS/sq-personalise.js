
var sq = (function (squiz) {
    'use strict';
/**
 * @module personalise
 * @author Chris Grist
 * @name personalise
 * @description Squiz personalise module
*/

    squiz.personalise = {};

    /**
     * @function
     * @memberof module:personalise
     * @description Core function for trait based personalisation filtering. 
     * Will take any array of objects with the appropriate audience fields, and match them to the user model provided in rules, returning a list in weighted order, and with non matching items removed.
     * @param list {object} - list of objects with audiences
     * @param rules {object} - list of current traits of the user model
     * @param options {object} - additional configuration options
     */ 
    squiz.personalise.filterObject = function(list, rules, options){
        var key, item, filteredList, audiences, matchWeighting;
        filteredList = [];

        // for each item in the list
        for (key in list){
            // are there audiences to match?
            item = list[key];
            audiences = getAudiences(item); 
            if (audiences){
                matchWeighting = matchAudiences(audiences, rules, options);
                if (matchWeighting > -1){
                    item.weight = matchWeighting;
                    filteredList.push(item);
                }   
            }else{
                filteredList.push(item);
            }
        }
        // sort filtered list
        filteredList = _.sortBy(filteredList, function(val,key){
            if (val.weight){
                // get the negative weight of the value, 
                // to fake a descending sort without having to reverse the array
                return val.weight - (val.weight * 2);
            }else{
                return 0;
            }
        });
        // return filtered list
        return filteredList;    
        return [];
    };

    /**
     * @function
     * @memberof module:personalise
     * @description confirm that valid audiences are present
     * @param item {object} - item from array that may or may not have audiences assigned
     */ 
    function getAudiences (item){
        var audiences;
        if (item.audiences){
            if (typeof(item.audiences) === "string"){
                // if its' a string, try and make it an object
                audiences = JSON.parse(item.audiences);
            }else{
                // otherwise assume it's already an object
                audiences = item.audiences;
            }
            if (audiences.length > 0){
                if (typeof(audiences[0].attr) !== "undefined"){
                    return audiences;
                }else{ 
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    function matchAudiences (audiences, rules, options){
        var key, passed, eachPassed, weighting, matched, mustOrCount, mustOrMatches;
        passed = true;
        weighting = 0;
        mustOrCount = 0;
        mustOrMatches = 0;
        for (key in audiences){
            // innocent until proven guilty
            eachPassed = true;
            // are any rules defined?
            if (typeof(rules) === "undefined"){
                // cannot pass if no rules on the user
                eachPassed = false;
            }else{
                // does this audience match?
                matched = matchAudience(audiences[key], rules, options, false);
                if (matched > -1){
                    // set a weighting
                    weighting = weighting + matched;
                    if (mustOrMatch(audiences[key])){
                        mustOrCount = mustOrCount + 1;
                        mustOrMatches = mustOrMatches + 1;
                    }
                } else if (mustNotMatch(audiences[key])){
                    // audience did NOT match, but SHOULD NOT match due to the inverted audience setting 'not match'
                    // set a weighting for our NOT matched audience, but sending the 'true' flag for not match
                    weighting = weighting + matchAudience(audiences[key], rules, options, true);
                    eachPassed = false;
                }else{
                    // did not pass
                    eachPassed = false;
                }
            }
            // if this audience did not pass - we need to decide what to do based on the options and
            // whether this audience insists that it passes, or 'at least one' of the audiences passes
            if (!eachPassed){
                // did this audience need to match to pass all audiences?
                if (mustMatch(audiences[key])){
                    // fail
                    passed = false;
                }else if(mustOrMatch(audiences[key])){
                    mustOrCount = mustOrCount + 1;
                }
            // if this audience did pass - we need to prevent it passing now if it says it MUST NOT PASS 
            }else{
                if (mustNotMatch(audiences[key])){
                    // fail
                    passed = false;
                }
            }
        }
        // if there are any audiences where at least one must match (e.g. 'MustOr') 
        // and this condition has not been met, this item has not passed
        if (mustOrCount > 0 && mustOrMatches < 1){
            passed = false;
        }
        if (passed){
            return weighting;
        }else{
            return -1;
        }
    }
    function matchAudience (audience, rules, options, mustNot){
        var matches, key;

        // for each attribute of the audience
        // does it match?
        // to allow not matches - a second call is made with the mustNot option set to true
        //      to invert the condition to retrive the weight, after first getting 
        //      the negative response to confirm it is a not match

        matches = true;
        for (key in audience.attr){
            if (typeof(rules[key]) !== "undefined"){
                if (matchAttribute(audience.attr[key], rules[key], key)){
                    // passed
                }else{
                    matches = false;
                }
            }else{
                // no matching key in the rules
                // exception for special case of 'no-date-set' for dates which are not set
                if (audience.attr[key] === "no-date-set"){
                    // passed
                }else if(audience.attr[key] === false){
                    // this could be a boolean value so, if the rule doesn't exist we are good
                        // passed
                }else{
                    matches = false;
                }
            }
        }
        if (matches && typeof(audience.weight) !== "undefined"){
            // audience matched and there is a weight to return
            return audience.weight;
        }else if(matches){
            // audience matched but there is not a weight to return
            return 0;
        }else if (!matches && mustNot && typeof(audience.weight) !== "undefined"){
            // audience did not match, but it should not, and there is a weight to return
            return audience.weight;
        }else if (!matches && mustNot){
            // audience did not match, but it should not, and there is no weight to return
            return 0;
        }else{
            // audience did not match - and we have not been instructed that it should not match
            // so we return a negative result
            return -1;
        }
    }

    // compare a rule to a value and return true or false
    function matchAttribute (attr, value, attributeName){
        var intersect, ruleDateStart, ruleDateEnd, ruleDateTimeStart, ruleDateTimeEnd, nowDate, nowTime, possibleDate;

        console.log(attr, value);

        possibleDate = false;
        if (typeof(value) !== "undefined"){
            if (_.isString(value)){
                if (value.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$/)){
                    possibleDate = true;
                }
            // is it a date type?
            }else if(Object.prototype.toString.call(value).match('Date')){
                // make sure it is not an invalid date
                if (!isNaN(value.getTime())){
                    possibleDate = true;
                }
            }
        }
        // special case for available date - this is compared against the current date - unless overridden in the settings
        if (attributeName === "AVAILABLE_DATE"){
            if (typeof(value) === "undefined"){
                nowDate = new Date();
            }else{
                nowDate = value;
            }
            if (typeof(attr.from) !== "undefined" && typeof(attr.to) !== "undefined"){
                ruleDateStart = new Date(attr.from);
                ruleDateEnd = new Date(attr.to);
                if (ruleDateStart && ruleDateEnd){
                    ruleDateTimeStart = ruleDateStart.getTime();
                    ruleDateTimeEnd = ruleDateEnd.getTime();
                    nowTime = nowDate.getTime();
                    // is now inside the date start and end?
                    if (nowTime <= ruleDateTimeEnd && nowTime >= ruleDateTimeStart){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    // audience not properly set - so defaulting to allow match
                    return true;
                }
            }else{
                // audience not properly set - so defaulting to allow match
                return true;
            }

        // special case if the target value would be a date, but the rule is set to 'no'
        } else if (attr === "no-date-set"){
            if (value){
                if (typeof(value.getDate) !== "undefined"){
                    if (!value.getDate()){
                        // invalid date
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    // undefined date
                    return true;
                }
                return true;
            }else{
                // value not set
                return true;

            }
        // special case for the target value is a date
        } else if (possibleDate){
            // because this is a date, we want to check against
            // yes, no, future, past values for this date
            switch(attr){
                case "yes": 
                    return true;
                    break;
                case "no":
                    // should only occur with cancelling a date in the testing tool
                    if (!value.getDate()){
                        return true;
                    }else{
                        return false;
                    }
                    break;
                case "future":
                    if (value.getTime() > new Date().getTime()){
                        return true;
                    }else{
                        return false;
                    }
                    break;
                case "past":
                    if (value.getTime() < new Date().getTime()){
                        return true;
                    }else{
                        return false;
                    }
                    break;
            }
        }else{

            // normalise attr to array
            if (typeof(attr) === "string"){
                // string
                //attr = [attr];
                attr = attr.split(/,\s*/);
            } else if (typeof(attr) === "boolean"){
                attr = [attr];
            }else if(Array.isArray(attr)){
                // array
                attr = attr;
            }else{
                // object
                //attr = _.keys(attr);
                attr = _.keys(_.pick(attr, function(v, k){ return v }));
            }

            // normalise value to array
            if (typeof(value) === "string"){
                //value = [value];
                value = value.split(/,\s*/);
            } else if (typeof(value) === "boolean"){
                value = [value];
            }else if (Array.isArray(value)){
                value = value;
            }else{
                // object
                value = _.keys(_.pick(value, function(v, k){ return v }))
            }

            // check whether the two arrays intersect (e.g. does one or more values match one or more of the rule attribute selected values)
            if (_.intersection(attr, value).length > 0){
                return true;
            }else{
                return false;
            }
        }
    }
    function mustMatch(audience){
        if (typeof(audience.restrict) !== "undefined"){
            if (audience.restrict === "Must"){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    function mustOrMatch(audience){
        if (typeof(audience.restrict) !== "undefined"){
            if (audience.restrict === "MustOR"){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    function mustNotMatch(audience){
        if (typeof(audience.restrict) !== "undefined"){
            if (audience.restrict === "Not"){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /* ------------------------------------------------------------------------------------ */


    /* ------------------------------------------------------------------------------------ */

    /**
     * @function
     * @memberof module:personalise
     * @description Will search the DOM for any trait based personalisation configured, and personalise sections of the page.
     * @param userdata {object} - the user data object, which should include a field called traits that contains the user traits.
     * @param options {object} - additional configuration options
     */ 
    squiz.personalise.filterPersonalisedAreasInDOM = function(userdata, options){
        var rules, i, personalisedSections, section, config;
        rules = userdata.traits;
        personalisedSections = squiz.personalise.getPersonalisedAreasFromDOM();

        // run basic show hide
        personaBasicShowHide(userdata, options);

        // more complex targeting
        // for eachpersonalised section
        for (i=0; i<personalisedSections.length; i++){
            section = personalisedSections[i];
            // get it's configuration
            config = section.dataset;   
            // depending on configuration
            switch(config.persbehaviour){
                // persona basic - simple match with a persona show or hide
                /*
                case "personab":
                    console.log('Behaviour: persona basic');
                    personaBasicShowHide(section, userdata, options);
                    break;
                */
                case "reorder":
                    console.log('Behaviour: reorder existing list');
                    reorderExistingList(section, config, rules, options);
                    break;
                case "jsonrender":
                    console.log('Behaviour: render list using json object and lodash templates');
                    renderJSONList(section, config, rules, options);
                    break;
                case "funnelbackquery":
                    console.log('Behaviour: load funnelback query and render display');
                    loadFunnelbackQuery(section, config, rules, options);
                    break;
                case undefined:
                    // ???
                    console.log('Undefined behaviour - nothing to personalise');
                    break;
                default:
                    // ????
                    console.log('Undefined behaviour - nothing to personalise [2]')
            }
        }
    };

    squiz.personalise.getPersonalisedAreasFromDOM = function(){
        var pers;
        pers = document.getElementsByClassName("sq-pers");
        return pers;
    };


    /**
     * @function
     * @memberof module:personalise
     * @description Main function for persona based personalisation. 
     * Taking the user data object, will check all content containers on the page for matching personas and show and hide the appropriate content.
     * @param userdata {object} - the user data object, which should include a field called code that represents the active persona code of the user.
     * @param options {object} - additional configuration options
     */ 
    function personaBasicShowHide(userdata, options){
        var userPersona, element, persona, show;
        // for each content container in SQ_CONTAINER_PERSONAS
        if (!_.isUndefined(userdata.code)){
            userPersona = userdata.code;
            console.log('user persona is: ' + userPersona);
        } else {
            userPersona = null;
            console.log('user has no persona set');
        }
        _.each(SQ_CONTAINER_PERSONAS, function(val,key){
            show = false;
            // get container
            element = document.getElementById('content_container_' + val.id);
            // only take first persona
            if (!_.isUndefined(val.persona)){
                if (val.persona.length < 1){
                    show = true;
                } else {
                    persona = val.persona[0];
                    // if its persona matches
                    if (userPersona === persona){
                        show = true;
                    }
                }
            }
            // add a class for highlighting
            if (element){
                if (!_.isUndefined(element.className)){
                    if (!element.className.match("sq-pers-element")){
                        element.className = element.className + " sq-pers-element ";
                    }
                    if (show){
                        element.style.display = 'block';
                        console.log('show element: content_container_' + val.id);
                    }else{
                        element.style.display = 'none';
                        console.log('hide element: content_container_' + val.id);
                    }
                }
            }
        });
            
    }

    function loadFunnelbackQuery(node, config, rules, options){
        var fb, fbfields, audiences, template, number, fburl, fbdata, fbresults, html, cardwidth;

        // get audiences
            // TODO
        // use personalisation to see if this should do anything in the first place
            // TODO

        // get the template to render the items
        template = node.nextElementSibling.innerHTML;
        template = _.template(template);

        // get count of items
        number = node.dataset.displaynumber;

        // get funnelback url
        fburl = node.dataset.fbquery;

        // map the funnelback fields
        // to do - improve how globals are handled
        fb = SQ_PERSONALISE_G.FUNNELBACK;
        fbfields = {
            "{{ fbdomain }}": fb.DOMAIN,
            "{{ fbcollection }}": fb.COLLECTION,
            "{{ fbprofile }}": fb.PROFILE
        };
        // get the user model and map the fields
        _.each(rules, function(val, key){
            var output = "[";
            _.each(val, function(val2, key2){
                output = output + val2 + " ";
            });
            output = output + "]";
            fbfields["{{" + key + "}}"] = output;
        });

        // replace all fields in the URL with the right values
        _.each(fbfields, function(val,key){
            fburl = fburl.replace(key, val);
        });

        fburl = fburl + fb.EXTRA;

        // are there any unreplaced fields? if so the query does not match and we should hide the section
        if (_.isNull(fburl.match("{{"))){
            // call url
            var xhr = new XMLHttpRequest();
            xhr.open('GET', fburl);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    console.log('fb data loaded');
                    //console.log(xhr.responseText);
                    // process data
                    // TODO better error checking on JSON data
                    fbdata = JSON.parse(xhr.responseText)
                    fbresults = _.get(fbdata, "response.resultPacket.results");
                    if (typeof(fbresults) !== "undefined"){
                        if (fbresults.length > 0){
                            // render display
                            html = "<div class='container my-2'><div class='row'>";
                            cardwidth = (100 / number) + "%";
                            _.each(fbresults, function(val,key){
                                if (key < number){
                                    val.cardwidth = cardwidth;
                                    html = html + template(val);
                                }
                            });
                            html = html + "</div></div>";
                            node.innerHTML = html;
                            node.style.display = 'block';
                            setTimeout(function(){
                                node.className = node.className.replace("sq-pers-hidden","");
                            }, 10);
                        }else{
                            console.log('Funnelback request returned no results [2]');
                            fbHideNode(node);
                        }
                    }else{
                        console.log('Funnelback request returned no results [1]');
                        fbHideNode(node);
                    }
                } else {
                    // handle error
                    console.log('Funnelback request failed.  Returned status of ' + xhr.status);
                    fbHideNode(node);
                }
            };
            xhr.send();
        }else {
            // reset the container
            fbHideNode(node);
        }
    }
    function fbHideNode(node){
        node.innerHTML = "";
        node.style.display = 'none';
        if (_.isNull(node.className.match("sq-pers-hidden"))){
            node.className = node.className + " sq-pers-hidden ";
        }        
    }

    function renderJSONList (node, config, rules, options){
        var scriptNode, items, filteredItems, template, templ, orderedClasses, html, obj, key;

        // get the items from the JSON script
        scriptNode = node.nextElementSibling.innerHTML;
        // TODO: add error checking
        items = JSON.parse(scriptNode);

        // filter the list
        filteredItems = sq.personalise.filterObject(items, rules, options);

        // compile the template from the DOM
        template = node.dataset.template;
        if (typeof(template) !== "undefined"){
            templ = _.template(template);
        }

        // organise ordered classes
        if (typeof(node.dataset.orderedClasses) !== "undefined"){
            orderedClasses = node.dataset.orderedClasses.split(' ');
        }else{
            orderedClasses = [];
        }     

        // output the list items
        html = "";
        for (key in filteredItems){
            obj = filteredItems[key];

            // add orderedClasses
            if (typeof(orderedClasses[key]) !== "undefined"){
                obj.orderedClass = orderedClasses[key];
            }else{
                obj.orderedClass = "";
            }

            html = html + templ(obj);
        }

        // set the HTML content in the node
        node.innerHTML = html;
    }

    function reorderExistingList (node, config, rules, options){
        var i, item, items, filteredItems, orderedClasses, key, maxItems;
        console.log('reorder existing list function');

        // organise ordered classes
        if (typeof(node.dataset.orderedClasses) !== "undefined"){
            orderedClasses = node.dataset.orderedClasses.split(' ');
        }else{
            orderedClasses = [];
        }

        // arrange the items from the DOM
        items = [];
        for (i=0; i<node.children.length; i++){
            console.log(node.children[i]);
            item = node.children[i];
            item.audiences = item.dataset.audiences;
            items.push(item);
        }

        // flter the list
        filteredItems = squiz.personalise.filterObject(items, rules, options);

        // truncate the list if we have max items - however ignore it if it's zero
        maxItems = parseInt(node.dataset.maxItems);
        if (maxItems > 0){
            filteredItems = filteredItems.slice(0, maxItems);
        }

        // for each filtered item
        for (i=0; i<filteredItems.length; i++){
            // do we need to adjust the classes on the node?
            // remove existing orderedClasses
            if (orderedClasses.length > 0){
                for (key in orderedClasses){
                    filteredItems[i].className = filteredItems[i].className.replace(key,"");
                }
                // add orderedClasses
                if (typeof(orderedClasses[i]) !== "undefined"){
                    filteredItems[i].className = filteredItems[i].className + " " + orderedClasses[i];
                }
            }
            // insert at start of list
            node.insertBefore(filteredItems[i], node.childNodes[i]);

            // remove display none
            filteredItems[i].style.display = '';
        }
        // for each item that is not in the filtered list
        for (i=0; i<items.length; i++){
            // if item is not in the filtered set, hide it
            if (filteredItems.indexOf(items[i]) < 0){
                items[i].style.display = 'none';
            }
        }
    }


    // given a set of rules (key value pairs about the user) find a variation with
    // a variation ID that matches x=y and show it
    squiz.personalise.setVariationBasedOnRules = function(rules, options){
        var templates, key, attr, keyval, rule;
        templates = $('[data-client-variation_id]')
        for (key in templates){
            if (typeof(templates[key].getAttribute) !== "undefined"){
                attr = templates[key].getAttribute('data-client-variation_id');
                keyval = attr.split('=');
                for (rule in rules){
                    if (rule == keyval[0] && rules[rule] == keyval[1]){
                        console.log("setting variation html:");
                        console.log(templates[key].innerHTML);
                        options.targetNode.html(templates[key].innerHTML);
                        //options.targetNode.innerHTML = templates[key].innerHTML;
                    }
                }
            }
        }
    };


    /* ------------------------------------------------------------------------------------
        Storage functions
    ------------------------------------------------------------------------------------ */


    squiz.personalise.getSqPersModelsFromStorage = function(){
        var userStored, modifiedStored;
        userStored = localStorage.getItem("sqpers");
        modifiedStored = localStorage.getItem("sqpers-m");
        try {
            userStored = JSON.parse(userStored);
        } catch (e) {
            userStored = {};
        }
        try {
            modifiedStored = JSON.parse(modifiedStored);
        } catch (e) {
            modifiedStored = {};
        }
        if (userStored === null){
            userStored = {traits: {}};
        }
        if (modifiedStored === null){
            modifiedStored = {traits: {}};
        }
        return {
            userStored: userStored,
            modifiedStored: modifiedStored
        };
    };
    squiz.personalise.setSqPersModelsToStorage = function(models){
        localStorage.setItem('sqpers', JSON.stringify(models.userStored));
        localStorage.setItem('sqpers-m', JSON.stringify(models.modifiedStored));
    };


    /* ------------------------------------------------------------------------------------
        Interface
    ------------------------------------------------------------------------------------ */

    /* to do : should not depend on jquery! */
   squiz.personalise.toggleHighlighting = function(){
        // for each  sq-pers-element 
        $('.sq-pers-element').each(function(key,val){
            // if element does not have ghost
            // add one
            if (!$(val).next('.sq-pers-element-ghost').length > 0){
                $(val).after('<div class="sq-pers-element-ghost"></div>');
            }
        })
        $('.sq-pers-element').toggleClass('active');
        $('.sq-pers-element-ghost').toggleClass('active');

        
    };  

    return squiz;
}(sq || {}));



/*
    Squiz personalise tracking
*/
var sq = (function (squiz) {
    'use strict';
    squiz.personalise = {};


    /**
     * @function
     * @memberof module:personalise
     * @description Track user activity - basd on tracking rules configured for the site
     * @param rules {object} - list of tracking rules
     */ 
    squiz.personalise.track = function(rules){
        var track;
        // get the tracking data
        track = getTrackingData();
        // for each rule 
        _.each(rules, function(rule,ruleKey){
            // figure out what kind of tracking to do
            if (!_.isUndefined(rule['tracking-source'])){
                // update the tracking data
                // review the tracking data
                    // update any trait that should be updated         
                switch(rule['tracking-source']){
                    case "visits": 
                        trackVisits(rule, ruleKey, track);
                        break;
                    case "track-sub-section":
                        trackSubSection(rule, ruleKey, track);
                        break;
                    case "track-metadata-tags":
                        trackMetadataTags(rule, ruleKey, track);
                        break;
                    case "referer":
                        trackReferer(rule, ruleKey, track);
                        break;
                    case "url-variable":
                        trackUrlVariable(rule, ruleKey, track);
                        break;
                    default:
                        break;
                }
            }
        });

        // calculate personas
        squiz.personalise.calculatePersonas();

    };

    squiz.personalise.calculatePersonas = function(){
        // get tracking data
        track = getTrackingData();
        // calculate the personas
        // set personas
    };



    function trackVisits(rule, ruleKey, track){
        var pathMatchRule, field;
        pathMatchRule = extractPathmatchRule(rule);
        value = window.location.pathname;
        // if pathmatch is set - track a specific path
        if (pathMatchRule){
            // does the current path match?
            if (value.match(pathMatchRule)){
                // track
                // if tracking output is increment, increment the value
                switch(rule['tracking-output']) {
                    case "increment":
                        if (rule['trait']){
                            incrementTrait(ruleKey, pathMatchRule);
                        }
                        break;
                    case "set":
                        break;
                    case "most-popular":
                        field = incrementPopular(ruleKey, pathMatchRule, value);
                        mostpopular = getMostPopular(ruleKey, pathMatchRule);
                        if (mostpopular){
                            setTrait(rule['trait'], mostpopular.r);
                        }
                        break;
                }
            }
        } else {

        }
        // otherwise - track the site
        // what do we want to do with it (tracking-output)
        // increment visits
        if (rule['tracking-output'] === "increment"){

            
        }else if (rule['tracking-output'] === "set"){

        }
    }



    function trackSubSection(rule, ruleKey, track){

    }

    function trackMetadataTags(rule, ruleKey, track){

    }

    function trackReferer(rule, ruleKey, track){

    }

    function trackUrlVariable(rule, ruleKey, track){

    }

    /**
    * Given a tracking rule ID and regex - track count of each value found. 
    */
    function incrementPopular(ruleKey, pathMatchRule, value){
        var pop, pathMatchText, field;
        pathMatchText = pathMatchRule.toString();
        pop = getStorage("sqpers-pop");

        // find out if we have already recorded some data on this topic
        field = _.find(pop.data, {"r": pathMatchText, "id": ruleKey});
        fieldIndex = _.findIndex(pop.data, {"r": pathMatchText, "id": ruleKey});
        if (field){
            // increment count for the given value 
            field.v = field.v + 1;
        } else{
            // create new field
            field = {
                "r": pathMatchText,
                "id": ruleKey,
                "v": 1
            }
        }
        return field;
    }
    /**
    * Given a tracking rule ID and regex - get the current most popular value
    */
    function getMostPopular(ruleKey, pathMatchRule){
        var pop, pathMatchText, field;
        pathMatchText = pathMatchRule.toString();
        pop = getStorage("sqpers-pop");

        // find out if we have already recorded some data on this topic
        field = _.find(pop.data, {"r": pathMatchText, "id": ruleKey});
        if (field){
            return field; 
        }else{
            return false;
        }
    }

    /**
    * Extracts the path-match regex from the rule, if present
    * @param {string} rule - the rule object to inspect.
    */
    function extractPathmatchRule(rule){
        if (!_.isUndefined(rule['path-match'])){
            if (rule['path-match'].length > 0){
                try {
                    return new RegExp(rule['path-match']);
                } catch(e) {
                    throw "Error extracting path match rule: invalid regular expression.";
                }
            }else{
                return false;
            }
        }else{
            return false;
        }

    }

    /**
    * Set the value of a trait, store it in local storage. Note does not currently allow for a callback.
    * 
    */
    function setTrait(trait, value){
        models = squiz.personalise.getSqPersModelsFromStorage();
        // manipulate models
        models.userStored.traits[trait] = value;
        squiz.personalise.setSqPersModelsToStorage(models);
    }

    /* ------------------- storage functions ------------------- */

    /**
    * Get tracking data (sqpers-tr) from browser localstorage
    */
    function getTrackingData(){
        var storage;
        storage = getStorage("sqpers-tr");
        if (storage){
            return storage;
        }else{
            storage = setTrackingData({});
            return storage;
        }
    }

    /**
    * Update tracking data (sqpers-tr) in browser local storage
    */
    function setTrackingData(data){
        return setStorage("sqpers-tr", data);
    }

    function getStorage(key){
        var storage;
        storage = localStorage.getItem(key);
        try {
            JSON.parse(storage);
            return storage;
        } catch (e) {
            console.log('Error parsing tracking data');
            return false;
        }
    }

    function setStorage(key, data){
        var storage;
        storage = {
            ts: Date.now(),
            data: data
        };
        text = JSON.stringify(storage);
        try{
            localStorage.setItem(localName, text);
            return storage;
        } catch (e){
            return false;
        }
    }


    return squiz;
}(sq || {}));