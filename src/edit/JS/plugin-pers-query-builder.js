/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */

/* ################################################################################################
#
#    Query Builder functions
#    
################################################################################################ */
//SQ_PERSONALISE_G.FUNNELBACK.FIELDS

EasyEdit.plugins.SqPersQueryBuilder = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', initSqPersQueryBuilder);
		$('body').on('click', '.sq-pers-add-query-button', addNewQueryMatchingRule)
		$('body').on('change', '.sq-pers-querytrait-select', changeQueryTraitSelect);
		$('body').on('change', '.sq-pers-edit-add-row__queryfixed', fixedQueryEntered);
		$('body').on('click', '.query-builder-rules .sq-pers-save', saveQueryRow);
		$('body').on('click', '.sq-pers-query-editor-preview__delete', deleteQueryRow);
	},// End init
}

function initSqPersQueryBuilder(){
    if( $('.sq-pers-query-builder').length > 0 ) {
		// For each instance of the switch ...
		$.each( $('.sq-pers-query-builder'), function( key, instance ) {
		    drawInitialQueryBuilder(instance, sqPersGetSchema(), sqPersGetFBSettings());
		});
    }
}

// Render the over all audience editor interface for an individual section
function drawInitialQueryBuilder(instance, schema, fbsettings){
    console.log('query builder');
    var $wrapper, audienceText, audience;
    //$wrapper = $(instance).find('.sq-metadata-contents-wrapper');
    $field = $(instance).children('.sq-form-field');
    $field.hide();
    // $wrapper.hide();
    
    // current rules:
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    friendlyQueries = sqPersQueriesToFriendlyQueries(queries, schema, fbsettings);

    // generate current rules 
    template = _.template($('#sq-pers-query-builder').html());
    output = template({"queries": friendlyQueries, "schema": schema, "fbsettings": fbsettings});
    $(instance).find('.sq-pers-editor-details').remove();
    $(instance).append(output);
}


// Add a new rule interface to configure an individual rule, click on "add audience matching rule" button
function addNewQueryMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-query-builder');
    $target = $container.find('.query-builder-rules');
    renderQueryMatchingRuleForm(sqPersGetSchema(), "", $target, {}, sqPersGetFBSettings());
}

function renderQueryMatchingRuleForm(schema, field, $target, options, fbsettings){
    //var output, audience, audienceText, data;
    
    // get the audience
    //audienceText = $target.closest('.sq-metadata-wrapper').find('.sq-form-field').val();
    //audience = sqPersAudienceTextToObj(audienceText);
    
    data = {
        "schema": schema, 
        "field": field, 
        "fbsettings": fbsettings
    };
    if (typeof(options) !== "undefined"){
        if (options.edit){
            data.edit = true;
        }else{
            data.edit = false;
        }
    }
    
    template =  _.template($('#sq-pers-edit-query-builder').html());
    output = template(data);
    $target.html(output);
}

// Re render the audience template after the user has selected a trait to match on
function changeQueryTraitSelect(e){
    var val;
    val = $(e.currentTarget).val();
    $container = $(e.currentTarget).closest('.sq-pers-editor-details');
    $target = $container.find('.query-matching-rules');    
    renderQueryMatchingRuleForm(sqPersGetSchema(), val, $target, {}, sqPersGetFBSettings());
}

function fixedQueryEntered(e){
    if ($(e.currentTarget).val().length > 0){
        $(e.currentTarget).closest('.form-group').find('.sq-pers-querytrait-select').attr('disabled', 'disabled');
    }else{
        $(e.currentTarget).closest('.form-group').find('.sq-pers-querytrait-select').removeAttr('disabled');
    }
}

function saveQueryRow(e){

    var trait, fixed, fbfield, queries, fbquery, $container, $field, instance;

    $container = $(e.currentTarget).closest('.sq-pers-edit-add-row__inner');
    
    instance = $container.closest('.sq-pers-query-builder');
    
    $field = instance.find('.sq-form-field');

    // get the trait
    trait = $container.find('.sq-pers-querytrait-select:enabled').val();
    
    // if undefined - then get the fixed value
    if (_.isUndefined(trait)){
        fixed = $container.find('.sq-pers-edit-add-row__queryfixed').val();
        if (_.isUndefined(fixed)){
            // can't save - error
            alert('Please select a trait or enter a fixed value.');
            return false;
        }else{
            trait = fixed;
        }
    } else {
        // wrap the trait in markers if it is from an audience trait - leave plain for hard coded
        trait = "{{" + trait + "}}";
    }
    
    fbfield = $container.find('.sq-pers-queryfield-select').val()
    if (_.isUndefined(fbfield)){
        alert('Please select a query field.');
        return false;
    }
    
    // get the queries
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    
    // save value to audience
    queries = sqPersAddQueryValueToQueries(queries, trait, fbfield);
    queryURL = sqPersQueriesObjToURL(queries);
    
    // save audience to text field
    $field.val(queryURL);
    
    // remove the interface
    drawInitialQueryBuilder(instance, sqPersGetSchema(), sqPersGetFBSettings());
}

function deleteQueryRow(e){
    var fbfield, queries, $instance, queryText, queryURL;
    
    // get the container for this instance of the query builder
    $instance = $(e.currentTarget).closest('.sq-pers-query-builder');
    
    // get the funnelback field name
    fbfield = $(e.currentTarget).closest('.sq-pers-current-state-preview__row').data('fbfield');
    
    // get the queries
    $field = $instance.find('.sq-form-field');
    queryText = $field.val();
    queries = sqPersQueryTextToObj(queryText);
    
    // update the queries
    queries = sqPersAddQueryValueToQueries(queries, "", fbfield);
    queryURL = sqPersQueriesObjToURL(queries);
    
    // save audience to text field
    $field.val(queryURL);
    
    // remove the interface
    drawInitialQueryBuilder($instance, sqPersGetSchema(), sqPersGetFBSettings());
}

function sqPersQueryTextToObj(queryText){
    if (_.isUndefined(queryText)){
        return [];
    }
    if (queryText.length < 1){
        return [];
    }
    texts = queryText.split('?')[1].split('&');
    pairs = [];
    _.each(texts, function(text, key){
        pair = text.split('=');
        if (pair[0] !== "profile" && pair[0] !== "collection" && pair[0] !== "query"){
            pairs.push(pair);
        }
    });
    return pairs;
}
function sqPersAddQueryValueToQueries(queries, trait, fbfield){
    var index;
    if (_.isUndefined(queries)){
        queries = [];
    }
    index = -1;
    _.each(queries, function(val,key){
        if (val[0] === fbfield){
            index = key;
        }
    });
    // if the field already exists in the array - update it - otherwise add a new row
    if (index > -1){
        // if the trait is specified - add or set it - otherwise remove it
        if (trait.length > 0){
            queries[index] = [fbfield, trait]    
        }else{
            queries.splice(index, 1);
        }
    }else{
        // only add the trait if it is specified
        if (trait.length > 0){
            queries.push([fbfield, trait]);
        }
    }
    return queries;
}
function sqPersQueriesObjToURL(queries){
    var fburl;
    //fburl = SQ_PERSONALISE_G.FUNNELBACK.DOMAIN + 
    //"?collection=" + SQ_PERSONALISE_G.FUNNELBACK.COLLECTION +
    //"&profile=" + SQ_PERSONALISE_G.FUNNELBACK.PROFILE +
    fburl = "{{ fbdomain }}?collection={{ fbcollection }}&profile={{ fbprofile }}&query=!null"; 
    _.each(queries, function(val,key){
       fburl = fburl + "&" + val[0] + "=" + val[1]; 
    });
    return fburl;
}

function sqPersQueriesToFriendlyQueries(queries, schema, fbsettings){
    var friendlyQueries = [];
    // for each query the user has created
    _.each(queries, function(query, key){
        // for each funnelback field in the schema, find the one that matches the query
       _.each(fbsettings.FIELDS, function(field, key2){
           // if the schema record matches the current query
            if (field.key == query[0]) {
                // if the trait is directly entered by the user
                if (_.isNull(query[1].match("{{"))){
                    field.trait = {"name": query[1], "key": query[1], "hardcoded": true};   
                }else{
                    field.trait = schema[query[1].replace('{{','').replace('}}','')];
                    field.trait.hardcoded = false;
                }
                friendlyQueries.push(field);
            }
       });
    });
    return friendlyQueries;
}