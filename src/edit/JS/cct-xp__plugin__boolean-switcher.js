/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

EasyEdit.plugins.BooleanSwitcher = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initBooleanSwitcher);

	},// End init

	// Hide what's required and listen for changes
	initBooleanSwitcher: function() {

		if( $('.onoffswitch').length > 0 ) {

			// For each instance of the switch ...
			$.each( $('.onoffswitch'), function( key, instance ) {

				// Set the labels
				var onLabel = $(instance).data('on-txt');
				var offLabel = $(instance).data('off-txt');

				// Tweak the markup
				$(instance).find('label').empty();
				$(instance).find('label').append('<span data-txt-off="' + offLabel + '" data-txt-on="' + onLabel + '" class="onoffswitch-inner"></span>');
				$(instance).find('label').append('<span class="onoffswitch-switch"></span>');

				var sweetSwitch = $(instance).html();

				sweetSwitch = sweetSwitch.replace('<ul>', '');
				sweetSwitch = sweetSwitch.replace('<li>', '');
				sweetSwitch = sweetSwitch.replace('</li>', '');
				sweetSwitch = sweetSwitch.replace('</ul>', '');

				$(instance).html( sweetSwitch );
			});
		}
	}
}