/**
 * Conditionally show/hide fields
 * Put data-listener="identifier" on the parent element
 * Put data-show="indetifier@value" on the parent element that you want to show
 * Put data-hide="indetifier@value" on the parent element that you want to hide
 */

EasyEdit.plugins.conditionalFields = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initConditionalFields);

	},// End init

	// Hide what's required and listen for changes
	initConditionalFields: function() {

		// Get all the instances of the templates on the page
		var instances = $('.matrix-options-template');

		// For each instance run the function within its scope
		$.each(instances, function( key, instance ) {
			// Execute the functions per instance
			doTheStuff( $(instance) );

		});

		// TODO: Need better name
		function doTheStuff( instance ) {

			var currentInstance = $(instance);

			// Get the listeners
			var listeners = $(currentInstance).find('[data-listener]');

			// Run actions for the listeners
			$.each(listeners, function(key, value) {

				var greaterThan = [];

				// Get the type
				var type = $(value).data('listener');

				// The items that are greater than
				var greaterItems = $(currentInstance).find('[data-show^="' + type + ':"]');

				// Get the selected value
				var selectedNow = $(value).find('input[type="text"], input[type="checkbox"]:checked, select').val();

				if( typeof selectedNow !== 'undefined' ) {

					selectedNow.toLowerCase().replace(' ','-');
				}

				// If it's a greater than listener type use the text not the value

				// NOTICE THE .TEXT() IT'S GETTING THE TEXT NOT THE VALUE ( CONSIDER .VAL() )
				if( type.indexOf('^gte') !== -1 ) {

					selectedNow = $(value).find('input[type="text"], input[type="checkbox"]:checked, select option:selected').text();
				}


				// Greater than show the fields
				// TODO: this could be extracted into function as it's simimlar in change event
				// ====================
				if( $.isNumeric( selectedNow ) ) {

					// Make any array of the greater than values
					$.each( greaterItems, function() {

						// Get just the number in the data attr
						var theNumber = $(this).data('show').substr( $(this).data('show').indexOf('^gte:') );

						theNumber = theNumber.replace('^gte:','');

						// Make into numbers
						theNumber = parseInt(theNumber);
						selectedNow = parseInt(selectedNow);

						// For the items that are greater than or equal to selected
						if( selectedNow >= theNumber ) {
							// Put in an array
							greaterThan.push( $(this).data('show') );
						}
					});

					// Hide them all
					$(currentInstance).find('[data-show^="' + type + ':"]').addClass('x-hide');

					// Show the ones that are greater than or equal to the input
					$.each( greaterThan, function( index ) {

						$(currentInstance).find( '[data-show="' + greaterThan[index] + '"]' ).removeClass('x-hide');
					});
				}

				// Hide the items
				// ====================
				// Show all
				$(currentInstance).find('[data-hide^="' + type + '@"]').removeClass('x-hide');
				// Hide items that are equal to assigned value
				$(currentInstance).find('[data-hide="' + type + '@' + selectedNow + '"]').addClass('x-hide');

				// Show the items
				// ====================
				// Hide all
				$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
				// Show the hidden items that are equal to assigned value
				$(currentInstance).find('[data-show="' + type + '@' + selectedNow + '"]').removeClass('x-hide');

				// If the listener is hidden hide the items
				// ====================
				var controlHidden = $(currentInstance).find('tr[data-listener="' + type + '"]').hasClass('x-hide');

				if( controlHidden ) {

					$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
					$(currentInstance).find('[data-hide^="' + type + '@"]').addClass('x-hide');
				}

				// The event listener for changes
				$(this).change( function() {

					// Get the selected value
					var selected = $(value).find('input[type="text"], input[type="checkbox"]:checked, select').val();

					if( typeof selected !== 'undefined' ) {

						selected.toLowerCase().replace(' ','-');
					}

					// If it's a greater than listener use the text not the value
					if( type.indexOf('^gte') !== -1 ) {

						selected = $(value).find('input[type="text"], input[type="checkbox"]:checked, select option:selected').text();
					}

					// For numeric values check the greater than
					// ====================
					if( $.isNumeric( selected  ) ) {

						// Cleat out the array
						greaterThan = [];

						// Make any array of the greater than values
						$.each( greaterItems, function() {

							// Get just the number
							var theNumber = $(this).data('show').substr( $(this).data('show').indexOf('^gte:') );

							theNumber = theNumber.replace('^gte:','');

							// Make into numbers
							selected = parseInt(selected);
							theNumber = parseInt(theNumber);

							// For the items that are greater than or equal to selected
							if( selected >= theNumber ) {
								// Put in an array
								greaterThan.push( $(this).data('show') );
							}
						});

						// Hide them all
						$(currentInstance).find('[data-show^="' + type + '"]').addClass('x-hide');

						// Show the ones that are greater than or equal to the input
						$.each( greaterThan, function( index ) {
							$(currentInstance).find( '[data-show="' + greaterThan[index] + '"]' ).removeClass('x-hide');
						});
					}

					// Hide the items
					// ====================
					// Show all (reset)
					$(currentInstance).find('[data-hide^="' + type + '@"]').removeClass('x-hide');
					// Hide items that are equal to assigned value
					$(currentInstance).find('[data-hide="' + type + '@' + selected + '"]').addClass('x-hide');

					// Show the items
					// ====================
					// Hide all (reset)
					$(currentInstance).find('[data-show^="' + type + '@"]').addClass('x-hide');
					// Show the hidden items that are equal to assigned value
					$(currentInstance).find('[data-show="' + type + '@' + selected + '"]').removeClass('x-hide');

				});

			}); // End loop

		} // End instance loop

	}
}





