/* ################################################################################################
#
#    DATA MANAGEMENT FUNCTIONS
#    
################################################################################################ */

function sqPersAudienceTextToObj (audienceText){
    if (typeof(audienceText) === "undefined"){
        return {};
    }
    if (audienceText === null){
        return {};
    }
    if (audienceText.length < 2){
        return {};
    }
    try {
        audienceObj = JSON.parse(audienceText);
        return audienceObj;
    } catch (e) {
        console.log("Error parsing audience json from metadata text field");
        return false;
    }
}

function sqPersAudienceObjToJSON (obj){
    // to do: normalise obj / json!
    return JSON.stringify(obj);
}

function sqPersAddRestrictionToAudience(audience, restriction){
    // to do: add options for setting restrictions
    if (_.isUndefined(restriction)){
        audience.restrict = "Must";
    }else{
        audience.restrict = restriction;
    }
    return audience;
}
function sqPersAddWeightToAudience(audience, weighting){
    weighting = weighting + 0;
    audience.weight = weighting;
    return audience;
}

function sqPersAddTraitValueToAudience (audience, trait, value){
    var remove = true;
    //to do: normalise value!
    if (typeof(audience['attr']) == "undefined"){
        audience['attr'] = {};
    }
    // if there is no value - we are removing the trait
    if (value !== null){
        if (!_.isUndefined(value)){
            if (value.length > 0){
                if (!_.isUndefined(value[0])){
                    if (value[0] !== ""){
                        remove = false;
                    }
                }
            }
        }    
    } 
    if (remove){
        delete audience['attr'][trait];
    } else {
        audience['attr'][trait] = value;
    }
    return audience;
}

function sqPersAddTraitValueToModel (model, trait, value){
    //to do: normalise value!
    // make sure we dont have an null model
    if (_.isNull(model)){
        model = {};
    }
    // in case there is no traits field - add one
    if (typeof(model['traits']) == "undefined"){
        model['traits'] = {};
    }
    // if the value is blank (or blank array) remove it from the model instead of keeping an empty field
    if (!Array.isArray(value) || !value.length){
        delete model.traits[trait];
    }else{
        model['traits'][trait] = value;
    }
    return model;
}


function sqPersGetSchema(){
    return schema;
}

function sqPersGetFBSettings(){
    return SQ_PERSONALISE_G.FUNNELBACK;
}