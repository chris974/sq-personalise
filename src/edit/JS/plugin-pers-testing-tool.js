/**
 * Adds a personalisation testing tool to the preview screen in Edit+
 * Expects ... 
 * 
 */


/* ################################################################################################
#
#    PERSONALISATION TESTING TOOL
#    
################################################################################################ */
SQ_PERS_TESTINGTOOL_USER_MODEL = {};
SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = {};
EasyEdit.plugins.SqPersTestingTool = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditAfterLoad', initSqPersTestingTool);
		$('body').on('click', '#ees_sqPersonalise_test__button, .testing-tool-header__close', toggleSqPersTestingTool);
		$('body').on('click', '.sq-pers-test-expand__button', toggleSqPersTestingToolSection);
		$('body').on('change', '.sq-pers-highlight-toggle', toggleHighlighting);
		$('body').on({mouseenter: hoverSqPersPersona, mouseleave: hoverSqPersPersona}, '.sq-pers-preset-link');

		$('body').on('click', '.testing-tool-body .sq-pers-sidebar-tab', toggleSqPersModifiedTraits);
		$('body').on('click', '.sq-pers-preset-link', selectSqPersPersona);
        $('body').on('click', '.persona-header__close', cancelSqPersPersona)
		
		$('body').on('change', '.sq-pers-test-field__input', updateSqPersTestFieldToModel);
		
		
	},// End init
}

function initSqPersTestingTool(e){
    var models;
    console.log('testing tool init');
    if (window.location.hash.match('mode=preview') !== null){
        // data init - get user models from storage
        models = getSqPersModelsFromStorage();
        SQ_PERS_TESTINGTOOL_USER_MODEL = models.userStored;
        SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = models.modifiedStored;
        
        if ($('#ees_sqPersonalise_test__button').length > 0){
        }else{
            
            $('#ees_previewOptions ul').append('<li class="toolbarButton noleft"><a href="javascript:void(0)" id="ees_sqPersonalise_test__button" class="" title="Personalisation testing tool"><span id="ees_sqPersonalise_test__button__name" class="icon"></span></a></li>');
            $('#ees_previewOptions .toolbarButton:not(:last-child)').addClass('noright');
        }
        if ($('#ees_sqpersonalise_test').length > 0){
        }else{
            $('#ees_toolBar').append('<div id="ees_sqpersonalise_test" style=""><h2>Personalisation testing tool</h2></div>');
        }
        console.log("THIS IS PREVIEW MODE");
        redrawSqPersTestingToolData();
    }
}

function toggleSqPersTestingToolSection(e){
    var $target;
    $target = $(e.currentTarget).closest('.sq-pers-test-field').find('.sq-pers-test-expand__section');
    if ($target.is(':visible')){
        // toggle arrow icons
        $('.sq-pers-test-expand__button .up').hide();
        $('.sq-pers-test-expand__button .down').show();
        $(e.currentTarget).find('.down').show();
        // toggle section
        $('.sq-pers-test-expand__section').slideUp();
    }else{
        // toggle arrow icons
        $('.sq-pers-test-expand__button .up').hide();
        $('.sq-pers-test-expand__button .down').show();
        $(e.currentTarget).find('.down').hide();
        $(e.currentTarget).find('.up').show();
        // hide any other open section
        $('.sq-pers-test-expand__section').slideUp();
        // show section
        $target.slideDown(function(){
            $('.sq-pers-test-field.active input').focus();
        });
        
    }
}

function toggleSqPersTestingTool(e){
    $('#ees_sqpersonalise_test').toggleClass('toolvisible');
    setSqPersFieldsFirstLastClasses();
}

function getSqPersModelsFromStorage(){
    // invoke function in front end personalisation code in iframe
    return document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.getSqPersModelsFromStorage();
}
function setSqPersModelsToStorage(models){
    // invoke function in front end personalisation code in iframe
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.setSqPersModelsToStorage(models);
}

// enable highlighting of elements - via front end code in iframe
function toggleHighlighting(e){
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.toggleHighlighting();
}

function updateSqPersTestFieldToModel(e){
    var value, model, trait;
    // get trait from field
    trait = getSqPersTraitKeyFromTestField(e);
    // get values
    value = getSqPersTraitValueFromTestField(e);
    // get model
    model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;
    // update the model
    model = sqPersAddTraitValueToModel(model, trait, value);
    SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED = model;
    // serialise the model to localStorage
    setSqPersModelsToStorage({
        userStored: SQ_PERS_TESTINGTOOL_USER_MODEL,
        modifiedStored: SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED
    })
    // redraw the interface
    redrawSqPersTestingToolData();
    
    // repersonalise the preview
    sqPersUpdatePage(model, {});
}
// given a field on change, extract the trait key
function getSqPersTraitKeyFromTestField(e){
    var $target = $(e.currentTarget);
    return $target.data('trait');
}

// given a field on change, extract the new value in the right format
function getSqPersTraitValueFromTestField(e){
    var $target = $(e.currentTarget);
    var result = [];
    if ($target.hasClass('select-multi')){
        _.each($target.find('input:checked'), function(val,key){
            result.push($(val).val()); 
        });
    }else if($target.hasClass('select-select')){
        result.push($target.find('input').val());
    }else if($target.hasClass('text-number')){
        result.push($target.find('input').val());
    }else if($target.hasClass('text-text')){
        result.push($target.find('input').val());
    }
    console.log(result);
    return result;     
}

function redrawSqPersTestingToolData(){
    var schema, model, expanded, presetindex;
    // get and compile the template
    template = _.template($('#sq-pers-testing-tool').html());
    
    // get the schema
    // TODO - don't assume this is here
    schema = SQ_PERSONALISE_G.DATA.SCHEMA;
    presets = SQ_PERSONALISE_G.DATA.PRESETS;
    

    
    // get list of field names to be expanded
    expanded = [];
    _.each($('.sq-pers-test-expand__section:visible .sq-pers-test-field__input'), function(val,key){
       expanded.push($(val).data('trait'));
    });
    
    // get active tab state
    tabstateall = $('.testing-tool-body .sq-pers-sidebar-tab.active').hasClass('sq-pers-all-traits');
    
    // the currently viewed profile/preset
    profile = null;
    profileset = false;
    if ($('.sq-pers-preset-link.enabled').length === 0){
        // check if there is a preset persona code in the model
        model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;
        if (!_.isUndefined(model.code)){
            profile = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {"code": model.code});
            profileset = true;
        }
    }else{
        presetindex = $('.sq-pers-preset-link').index($('.sq-pers-preset-link.enabled'));
        if (presetindex > 0){
            profile = presets[presetindex - 1];
            if (!_.isUndefined(profile.traits)){
                profileset = true;
            }
        } else {
            profile = {"traits": []};
        }            

        // switch the existing model to the profile model
        setSqPersModelsToStorage({
            userStored: SQ_PERS_TESTINGTOOL_USER_MODEL,
            modifiedStored: profile
        });
        // repersonalise the preview
        sqPersUpdatePage(profile, {});
    }

    // merge the audience and the model (getSqPersModelFromTestingTool)
    model = SQ_PERS_TESTINGTOOL_USER_MODEL_MODIFIED;

    // render the template - with the schema and audience
    output = template({"model": model, "schema": schema, "presets": presets, "expanded": expanded, "tabstateall": tabstateall, "profile": profile, "profileset": profileset});
    
    // set the html
    $('#ees_sqpersonalise_test').html(output);
    
    // setup the initial modified traits tab setting
    toggleSqPersModifiedTraits(undefined);
    
    // if visible, fix the rounded corners for first and last rows
    if ($('#ees_sqpersonalise_test:visible').length > 0){
        setSqPersFieldsFirstLastClasses();
    }
    
    // render boolean switcher fields
    renderBooleanSwitcher();

    // fade in any hidden elements
    $('.sq-pers__fadein').fadeIn(200);

    // make sure we're scrolled to where the user was immediately before redrawing
    $('.sq-pers-test-field.active input').focus();

}

function toggleSqPersModifiedTraits(e){
    console.log('toggle modified traits');
    if (typeof(e) !== "undefined"){
        $(e.currentTarget).parent().find('a').toggleClass('active');
    }
    if ($('.testing-tool-body .sq-pers-sidebar-tab.active').hasClass('sq-pers-all-traits')){
        // show all traits
        $('.sq-pers-test-field').show();
    }else{
        // show only modified traits
        $('.sq-pers-test-field').hide();
        $('.sq-pers-test-field.active').show();
    }
    setSqPersFieldsFirstLastClasses();
}

function setSqPersFieldsFirstLastClasses(){
    // remove then add first and last classes    
    $('.sq-pers-test-fields').each(function(key,val){
        $(val).find('.sq-pers-test-field').removeClass('last first');
        $(val).find('.sq-pers-test-field:visible:last').addClass('last');
        $(val).find('.sq-pers-test-field:visible:first').addClass('first');
    });
}

/* ################################################################################################
#
#    PRESETS
#    
################################################################################################ */

function hoverSqPersPersona(e){
    //$(e.currentTarget).find('.sq-pers-present-link__full').toggle();
    $(e.currentTarget).toggleClass('hovered');
    $(e.currentTarget).closest('.sq-pers-preset-list').toggleClass('active');
}
function selectSqPersPersona(e){
    console.log('selecting persona');
    //$(e.currentTarget).children().toggle().toggleClass('enabled');
    if ($(e.currentTarget).hasClass('enabled')){
        $(e.currentTarget).closest('.sq-pers-preset-list').find('.enabled').removeClass('enabled');
    }else{
        $(e.currentTarget).closest('.sq-pers-preset-list').find('.enabled').removeClass('enabled');
        $(e.currentTarget).addClass('enabled');

        // trigger redraw!
        redrawSqPersTestingToolData();
    }
}
function cancelSqPersPersona(e){
    $('.sq-pers-preset-list').removeClass('enabled');
    $('.sq-pers-preset-link--null').addClass('enabled');
    redrawSqPersTestingToolData();
}

/* ################################################################################################
#
#    Re-render page
#    
################################################################################################ */

function sqPersUpdatePage(userdata, options){
    document.getElementById("ees_modePreviewFrame").contentWindow.sq.personalise.filterPersonalisedAreasInDOM(userdata, options);
}

/* ################################################################################################
#
#    Copy of boolean switcher from CCT XP container templates
#    
################################################################################################ */
function renderBooleanSwitcher() {
    if ($('.onoffswitch').length > 0) {
        $.each($('.onoffswitch'), function(key, instance) {
            var onLabel = $(instance).data('on-txt');
            var offLabel = $(instance).data('off-txt');
            $(instance).find('label').empty();
            $(instance).find('label').append('<span data-txt-off="' + offLabel + '" data-txt-on="' + onLabel + '" class="onoffswitch-inner"></span>');
            $(instance).find('label').append('<span class="onoffswitch-switch"></span>');
            var sweetSwitch = $(instance).html();
            sweetSwitch = sweetSwitch.replace('<ul>', '');
            sweetSwitch = sweetSwitch.replace('<li>', '');
            sweetSwitch = sweetSwitch.replace('</li>', '');
            sweetSwitch = sweetSwitch.replace('</ul>', '');
            $(instance).html(sweetSwitch);
        });
    }
}