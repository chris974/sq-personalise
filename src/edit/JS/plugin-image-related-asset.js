/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

console.log('test 1');
EasyEdit.plugins.SqPersImageRelatedAssetViewer = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
	    console.log('test 2');
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initImageRelatedAssetViewer);
		$('body').on('change', 'sq-image-related-asset-viewer input[name$="_assetid"]', testO)

	},// End init

	// Hide what's required and listen for changes
	initImageRelatedAssetViewer: function() {
		if( $('.sq-image-related-asset-viewer').length > 0 ) {

			// For each instance of the switch ...
			$.each( $('.sq-image-related-asset-viewer'), function( key, instance ) {
			      console.log('found sq-image-related-asset-viewer');
			      
			      assetid = $(instance).find('input[name$="_assetid"]').val();
			      target = $(instance).find('.sq-image-related-asset-viewer__img');
			      sqImageRelatedAssetViewerUpdate(assetid, target);
			      
			      $(instance).find('input[name$="_assetid"]').on('change', function(e){
			          assetid = $(e.currentTarget).val();
			          sqImageRelatedAssetViewerUpdate(assetid, $(e.currentTarget).closest('.sq-image-related-asset-viewer').find('.sq-image-related-asset-viewer__img'))
			      });
			});
	    
		}
	}
}

function testO(e){
    console.log(TESTO);
}

function sqImageRelatedAssetViewerUpdate(assetid, target){
    var baseurl, url;
    baseurl = window.location.href.replace(/_edit.*/, '');
    url = baseurl + '?a=' + assetid;
    if (assetid > 0){
        $(target).css('background-image', 'url("' + url + '")');
        $(target).removeClass('screenHide');
    }else{
        $(target).addClass('screenHide');
    }
    console.log('TEST2');
}
