/**
 * Toggles content on click
 */

EasyEdit.plugins.initTemplateMetadata = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initMetadataClick);

	},// End init

	initMetadataClick: function() {

		var toggleWrapper = $('.form-toggle');

		$.each(toggleWrapper, function(key, value) {

			var toggleButton	= $( $(value).find('.form-toggle-btn') );
			var tableWrapper	= $( $(value).find('.form-toggle-body') );
			var sectionHeading	= $( $(value).find('.section-heading') );

			sectionHeading.click(function() {

				tableWrapper.slideToggle();

				$(this).toggleClass('open');

				if (toggleButton.val() == 'Show Options') {

					toggleButton.val('Hide Options');

				} else {

					toggleButton.val('Show Options');
				}

			});
		});
	}
}