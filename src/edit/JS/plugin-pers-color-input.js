/**
 * Turns checkbox into a switch thing
 * Expects the checkbox to be wrapped in <div class="onoffswitch"></div>
 * Expects the layout to be list NOT table in Matrix Settings
 */

EasyEdit.plugins.ImageRelatedAssetViewer = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', self.initSqPersEditColorInput);
	},// End init

	// Hide what's required and listen for changes
	initSqPersEditColorInput: function() {
	    console.log('color plugin');
	    $('.sq-pers-edit-color-input').find('input').each(function(key,val){
            $(val).attr('type', 'color');
	    });
	}
}
