/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */

/*
    schema = {
        "key": {
            name: "Friendly name",
            key: "key",
            type: "select" | "text",
            subtype: "multi" | "boolean" | "number",
            note: "Helpful note for editing and testing tools",
            example: "Helpful example for editing tool",
            showInTest: "Y" | "N",
            options: { "key": "Value", "key2": "Value 2"} // only for selects
        }
    }

*/

var schema, presets;
if (_.isUndefined(SQ_PERSONALISE_G)){
    SQ_PERSONALISE_G = {};   
}
schema = _.get(SQ_PERSONALISE_G, "DATA.SCHEMA");
presets = _.get(SQ_PERSONALISE_G, "DATA.PRESETS");

/* ################################################################################################
#
#    AUDIENCE EDITOR FUNCTIONS
#    
################################################################################################ */

EasyEdit.plugins.SqPersAudienceEditor = {

	/**
	 * Initialise the plugin
	 */
	init: function()
	{
		var self = this;
		// Add a function to the after ees load event
		EasyEditEventManager.bind('EasyEditScreenLoad', initSqPersAudienceEditor);
		$('body').on('change', '.sq-pers-trait-select', changeTraitSelect);
		$('body').on('change', '.sq-pers-add-trait-value', changeTraitValue);
		$('body').on('click', '.sq-pers-save-new-trait-matching-rule', saveNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-add-audience-button', addNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-cancel-new-trait-matching-rule', cancelNewTraitMatchingRule);
		$('body').on('click', '.sq-pers-audience-preview__edit', editExistingTraitMatchingRule);
		$('body').on('change', '.sq-pers-audience-weighting', updateWeightingSliderOutput);
		$('body').on('change', '.sq-pers-audience-preview__logic-list', updateRestriction);
        $('body').on('click', '.sq-pers__edit-target', showPersonaSelector);
        $('body').on('click', '.sq-pers__modal .closeModal', sqPersCloseModal);
        $('body').on('click', '.sq-pers__persona-list__persona', sqPersEditSwitchPersona);
        $('body').on('click', '#sq-pers__save-persona', sqPersEditSavePersona);
	},// End init
}

function initSqPersAudienceEditor(){
    
    // add the button to any container templates
    // for each container
    targetTemplate = _.template($('#sq-pers-edit-target-button').html());
    schema = sqPersGetSchema();
    assetID = $('.ees-tab-screen__tab').data('id');
    $('.bodycopy_tools.editable').each(function(key,val){
        // if set to block level
            // TODO: check if block level
            // get the metadata of the asset
            // get position
            var position = -1;
            _.each($(val).closest('.bodycopy_manager_div').attr('class').split(' '), function(classVal,classKey){
                if (classVal.indexOf('position-') === 0){
                    if (typeof(classVal) !== "undefined"){
                        position = classVal.split('-')[1];
                    }
                }
            });
            // get asset ID
            if (position > 0){
                var bodyCopy = EasyEditScreens.screen.contentPageStandard.bodycopyManagers[assetID].manager.getDivByPosition(position);
                var bodyCopyID = bodyCopy.id;
                if (typeof(bodyCopyID) !== "undefined"){
                    EasyEditAssetManager.getMetadata({id: bodyCopyID}, function(meta){ 
                        var obj, personaData;
                        console.log(meta); 
                        obj = {};
                        obj.assetid = bodyCopyID;
                        // if the metadata has personalisation settings set - add them to the object
                        if (typeof(meta) !== "undefined"){
                            if (typeof(meta["target.personas"]) !== "undefined"){
                                personaData = JSON.parse(meta["target.personas"]);
                                if (typeof(personaData[0]) !== "undefined"){
                                    obj.personacode = personaData[0];
                                    obj.persona = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {code: obj.personacode});
                                }
                            }
                        }
                        if (!obj.persona){
                            obj.persona = null;
                        }
                        // get the template
                        output = targetTemplate(obj);
                        // render the button
                        $(val).find('.bodycopy_properties.icon').after(output);
                    });            
                }
            }
        // else
            // add button with warning
    });

    // for container templates
    if( $('.sq-pers-audience-editor').length > 0 ) {
		// For each instance of the switch ...
		$.each( $('.sq-pers-audience-editor'), function( key, instance ) {
		    drawInitialAudienceEditor(instance, sqPersGetSchema());
		});
    }
    
    // for asset level metadata
    if ($('#ees_editMetadata').length > 0){
        if (!_.isUndefined(SQ_PERSONALISE_G.DATA.FIELD)){
            sqPersAudField = $('#metadata_field_text_' + SQ_PERSONALISE_G.DATA.FIELD + '_value');
            if (sqPersAudField.length > 0){
                instance = $(sqPersAudField).closest('.cellData');
                instance.addClass('sq-pers-audience-editor');
                drawInitialAudienceEditor(instance, sqPersGetSchema());
            }
        }
    }
}

// Render the over all audience editor interface for an individual section
function drawInitialAudienceEditor(instance, schema){
    var $wrapper, field, audienceText, audience;
    console.log('audience editor');
    field = $(instance).children('.sq-form-field');
    field.hide();
    // $wrapper = $(instance).find('.sq-metadata-contents-wrapper');
    // $wrapper.hide();
    
    // current rules:
    audienceText = field.val();
    audience = sqPersAudienceTextToObj(audienceText);
    audience = sqPersPurgeMissingFields(audience, schema);

    // generate current rules 
    template = _.template($('#sq-pers-edit-audience').html());
    output = template({"audience": audience, "schema": schema});
    $(instance).find('.sq-pers-audience-editor-details').remove();
    $(instance).append(output);
}

function showPersonaSelector(e){
    assetid = $(e.currentTarget).closest('.sq-pers__edit-target').data('assetid');
    modal = new EasyEditModal("Assign persona", "Select the persona to target this content container to.",'sq-pers__modal__persona-selector','sq-pers__modal');

    // generate current rules 
    template = _.template($('#sq-pers-persona-selector').html());
    current = "international";
    output = template({"personas": SQ_PERSONALISE_G.DATA.PRESETS, "current": current, "assetid": assetid});

    modal.addContent(output);


    html = modal.getTemplate();
    $('body').remove('#sq-pers__modal__persona-selector');

    // Remove any old overlay information
    $('#ees_assetFinderOverlay').remove();
    $('body').prepend('<div id="ees_assetFinderOverlay"></div>');
    var $container = jQuery('#ees_assetFinderOverlay');
    $container.html(html);

}
function sqPersCloseModal(e){
    $('.sq-pers__modal').remove();
    $('#ees_assetFinderOverlay').remove();
}
function sqPersEditSwitchPersona(e){
    $('.sq-pers__persona-list__persona').removeClass('selected');
    $(e.currentTarget).addClass('selected');
}
function sqPersEditSavePersona(e){
    //set the relevant metadata
    currentPersona = $('.sq-pers__persona-list__persona.selected').data('code');
    sqpersJSAPI = new EasyEditAPI();
    assetid = $('.sq-pers__persona-list').data('assetid');
    fieldid = "1993";
    $('#ees_assetFinderOverlay').remove();
    EasyEditOverlay.showOverlay('loading');
    sqpersJSAPI.JSAPI.setMetadata({
       "asset_id": parseInt(assetid),
       "field_id": parseInt(fieldid),
       "field_val": '["' + currentPersona + '"]',
    }).done(function(data){
        console.log(data);
        // change the display in the body copy
        obj = {};
        obj.assetid = assetid;
        obj.personacode = currentPersona;
        obj.persona = _.find(SQ_PERSONALISE_G.DATA.PRESETS, {code: obj.personacode});
        targetTemplate = _.template($('#sq-pers-edit-target-button').html());
        output = targetTemplate(obj);
        $marker = $('.sq-pers__edit-target--' + assetid).prev('.bodycopy_properties');
        $('.sq-pers__edit-target--' + assetid).remove();
        $marker.after(output);
        EasyEditOverlay.hideOverlay('loading');
        // cancel the modal            
    });
}


function sqPersPurgeMissingFields(audience, schema){
    for (key in audience.attr){
        if (typeof(schema[key]) === "undefined"){
            delete audience.attr[key];
        }
    }
    return audience;
}

function audienceSchemaFieldToFormRow(schema, field, $target, options){
    var output, audience, audienceText, data;
    
    // get the audience
    audienceText = $target.closest('.sq-pers-audience-editor').children('.sq-form-field:not([name$=_default])').val();
    audience = sqPersAudienceTextToObj(audienceText);
    
    data = {
        "schema": schema, 
        "field": field, 
        "audience": audience
    };
    if (typeof(options) !== "undefined"){
        if (options.edit){
            data.edit = true;
        }else{
            data.edit = false;
        }
    }
    
    template =  _.template($('#sq-pers-edit-audience-matching').html());
    output = template(data);
    $target.html(output);
}


// 
function saveNewTraitMatchingRule(e){
    var value, audience, trait, $field, wrapper, fieldType, restriction, weighting;

    // serialise changes and update JSON field 
    $container = $(e.currentTarget).closest('.audience-matching-rules-inner').find('.sq-pers-add-trait-value');
    
    // get the top level wrapper
    wrapper = $container.closest('.sq-pers-audience-editor');
    
    // get the trait
    trait = $container.data('key');
    
    // get the form field
    $field = wrapper.children('.sq-form-field:not([name$=_default])');
    
    // get the current audience
    audienceText = $field.val();
    audience = sqPersAudienceTextToObj(audienceText);
    
    // get the restriction type
    restriction = wrapper.find('.sq-pers-audience-preview__logic-list').val();
    
    // get the weighting
    weighting = parseInt(wrapper.find('.sq-pers-audience-weighting').val());
    
    // different processing for different field types
    fieldType = $container.data('fieldType');
    if (fieldType === "select-multi"){
        val = $container.find('input.sq-pers-trait-checkbox[type=checkbox]:checked').map(function(_, el) {
            return $(el).val();
        }).get();
        value = val;    
    }else if (fieldType === "select-select"){
        value = [$container.find('select').val()];
    }else{
        value = [$container.find('input').val()];
    }
    
    // save value to audience
    audience = sqPersAddTraitValueToAudience(audience, trait, value);
    audience = sqPersAddRestrictionToAudience(audience, restriction);
    audience = sqPersAddWeightToAudience(audience, weighting);
    json = sqPersAudienceObjToJSON(audience);
    
    // in case we are in metadata screen mode, trigger a click on the use default checkbox if it is checked
    $container.closest('.sq-pers-audience-editor').find('input[name$=_default][checked=checked]').trigger('click');
    // enable the save button
    $('#ees_saveButtonAction').removeClass('disabled');    
    
    // save audience to text field
    $field.val(json);
    
    // remove the interface
    // $(e.currentTarget).closest('.audience-matching-rules-inner').remove();
    drawInitialAudienceEditor(wrapper, sqPersGetSchema());
}

function updateWeightingSliderOutput(e){
    $(e.currentTarget).parent().find('.weighting-output').text($(e.currentTarget).val());
    // if value set - active add button
    if ($(e.currentTarget).closest('.sq-pers-edit-add-row').find('.sq-pers-add-trait-value input:checked, .sq-pers-add-trait-value select:selected, .sq-pers-add-trait-value input[type=text]').val().length > 0){
        changeTraitValue();
    }
}
function updateRestriction(e){
    // if value set - activate add button   
    if ($(e.currentTarget).closest('.sq-pers-edit-add-row').find('.sq-pers-add-trait-value input:checked, .sq-pers-add-trait-value select:selected, .sq-pers-add-trait-value input[type=text]').val().length > 0){
        changeTraitValue();
    }
}
// cancel creation of a new audience matching rule
function cancelNewTraitMatchingRule(e){
    $(e.currentTarget).closest('.audience-matching-rules-inner').remove();
}

// Show the add button after the user has selected a value to match on the given trait
function changeTraitValue(e){
    // show the add button
    $('.sq-pers-save-new-trait-matching-rule').show();
}

// Re render the audience template after the user has selected a trait to match on
function changeTraitSelect(e){
    var val;
    val = $(e.currentTarget).val();
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');    
    audienceSchemaFieldToFormRow(sqPersGetSchema(), val, $target, {});
}

// Add a new rule interface to configure an individual rule, click on "add audience matching rule" button
function addNewTraitMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');
    audienceSchemaFieldToFormRow(sqPersGetSchema(), "", $target, {});
}

// Open the rule interface to configure an individual rule, with existingvalues
function editExistingTraitMatchingRule(e){
    $container = $(e.currentTarget).closest('.sq-pers-audience-editor-details');
    $target = $container.find('.audience-matching-rules');
    field = $(e.currentTarget).data('traitKey');
    audienceSchemaFieldToFormRow(sqPersGetSchema(), field, $target, {edit: true});
}


