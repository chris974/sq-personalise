/**
 * Replaces a text field with an audience editor interface
 * Expects ... 
 * 
 */
var schema, presets;
if (_.isUndefined(SQ_PERSONALISE_G)){
    SQ_PERSONALISE_G = {};   
}
schema = _.get(SQ_PERSONALISE_G, "DATA.SCHEMA");
presets = _.get(SQ_PERSONALISE_G, "DATA.PRESETS");

/* ################################################################################################
#
#    JSON LIST EDITOR PLUGIN (user data schema, personas, tracking rules)
#    
################################################################################################ */

EasyEdit.plugins.SqPersJsonListEditor = {

    /**
     * Initialise the plugin
     */
    init: function()
    {
        var self = this;
        // Add a function to the after ees load event
        EasyEditEventManager.bind('EasyEditScreenLoad', SqPersJLE.jsonListEditor);
        $('body').on('click', '.sq-pers-json-list-editor__display .section-heading', SqPersJLE.toggleItem);
        $('body').on('change', '.sq-pers-json-list-editor__display [data-listen] input, .sq-pers-json-list-editor__display [data-listen] select', SqPersJLE.listenerChanged);
        $('body').on('click', '.sq-pers-json-add-row', SqPersJLE.addRow);
        $('body').on('change', '.sq-pers-json-keyval', SqPersJLE.keyvalChange);
        $('body').on('click', '.sq-pers-json-keyval__remove', SqPersJLE.keyvalRemove);
        $('body').on('change', '.sq-pers-json__val', SqPersJLE.enableUpdateButton);
        $('body').on('click', '.sq-pers-save-row', SqPersJLE.saveRow);
        $('body').on('click', '.sq-pers-cancel-row', SqPersJLE.cancelRow);
        /*
        $('body').on('change', '.sq-pers-trait-select', changeTraitSelect);
        $('body').on('change', '.sq-pers-add-trait-value', changeTraitValue);
        $('body').on('click', '.sq-pers-save-new-trait-matching-rule', saveNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-add-audience-button', addNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-cancel-new-trait-matching-rule', cancelNewTraitMatchingRule);
        $('body').on('click', '.sq-pers-audience-preview__edit', editExistingTraitMatchingRule);
        $('body').on('change', '.sq-pers-audience-weighting', updateWeightingSliderOutput);
        $('body').on('change', '.sq-pers-audience-preview__logic-list', updateRestriction);
        */
    },// End init
    
}
// SqPersJsonListEditor
var SqPersJLE = {
    jsonListEditor: function(e){
        var self = this;
        // for each editor
        if( $('.sq-pers-json-list-editor').length > 0 ) {
            // For each instance of the editor
            $.each( $('.sq-pers-json-list-editor'), function( key, instance ) {
                $(instance).find('.sq-pers-json-list-editor__field .sq-form-field').hide();
                SqPersJLE.drawJsonListEditor(instance, null, false);
            });
        }
    },
    drawJsonListEditor: function(instance, active, add){
        var schema, data, namefield, categoryfield;
        console.log("drawJsonListEditor");
        
        // get data and schema to figure out what to draw
        data = $(instance).find('.sq-form-field').val();
        schema = $(instance).next('.sq-pers-json-list-editor__schema').html();
        categoryfield = $(instance).data('categoryfield');
        namefield = $(instance).data('namefield');
        keyfield = $(instance).data('keyfield');
        type = $(instance).data('type');
        
        if(_.isUndefined(data) || _.isUndefined(schema)){
            return false;
        }
        
        if (data !== ""){

            data = JSON.parse(data);
            schema = JSON.parse(schema);

            // if adding a new row
            if (add){
                if (type === "object"){
                    data.NEW_ROW = {};
                    active = "NEW_ROW";
                }else{
                    data.push({});
                    active = data.length;
                }
            }
            
            // for each row, draw an accordion - pass through the data, the schema and whether a particular row is active
            template = _.template($('#sq-pers-json-list-editor__template').html());
            templateData = {
                data: data, 
                schema: schema, 
                active: active, 
            }
            if (!_.isUndefined(namefield)){
                templateData.namefield = namefield;
            }
            if (!_.isUndefined(namefield)){
                templateData.categoryfield = categoryfield;
            }
            if (!_.isUndefined(keyfield)){
                templateData.keyfield = keyfield;
            }
            output = template(templateData);
            $(instance).find('.sq-pers-json-list-editor__display').remove();
            $(instance).append(output);
            
            // handle show if
            SqPersJLE.handleShowIf(instance);
            
            // focus if a section is open
            $(instance).find('.form-toggle-body:visible input').first().focus();
            
            // broadcast event that json list editor is drawn
            //$(instance).trigger("json-list-editor-rendered");
            $(instance).trigger("json-list-editor-rendered");
        }            
    },  
    toggleItem: function(e){
        value = $(e.currentTarget).closest('.form-toggle');
        var toggleButton = $($(value).find('.form-toggle-btn'));
        var tableWrapper = $($(value).find('.form-toggle-body'));
        var sectionHeading = $($(value).find('.section-heading'));
        var updateButton = $(tableWrapper).find('.sq-pers-save-row');
        if (updateButton.is(':not([disabled])')){
            confirmed = false;
            conf = confirm('If you continue you will lose changes in the currently expanded section. Do you want to continue?');
            if (conf){
                confirmed = true;
            }
        }else{
            confirmed = true;
        }

        if (confirmed){
            up = false;
            if (toggleButton.val() == '▼'){
                up = true;
            }
            
            // hide the other items
            list = $(e.currentTarget).closest('.sq-pers-json-list-editor__display');
            $(list).find('.section-heading').removeClass('open');
            $(list).find('.form-toggle-body').slideUp();
            $(list).find('.form-toggle-btn').val('▼');
            
            if (up){
                tableWrapper.slideDown();
                sectionHeading.addClass('open');
                if (toggleButton.val() == '▼') {
                    toggleButton.val('▲');
                } else {
                    toggleButton.val('▼');
                }
            }
        }
    },
    handleShowIf: function(instance){
        // for each show - hide it
        $(instance).find('[data-show]').hide();
        $(instance).find('[data-show]').each(function(key,showIfInstance){
            // which section is the field in?
            section = $(showIfInstance).closest('.form-toggle-body');
            // which field is it listening to?
            listenTo = $(showIfInstance).data('show');
            // if we have a value it is listening to...
            if (!_.isUndefined(listenTo)){
                // split value to get field and value
                listenTo = listenTo.split('@');
                // find the appropriate listener within this section
                listener = $(section).find('[data-listen^=' + listenTo[0] + '] .sq-pers-json__val');
                // check if the listener's current value matches the value
                if ($(listener).val() === listenTo[1]){
                    $(showIfInstance).show();
                }
            }
        });
    },
    listenerChanged: function(e){
        // item has changed
        // check for anything listening to this
        container = $(e.currentTarget).closest('.sq-pers-json-list-editor__display');
        listenVal = $(e.target).closest('.form-group').data('listen');
        target = $(container).find('[data-show^=' + listenVal + ']');
        $(target).toggle();
    },
    addRow: function(e){
        instance = $(e.currentTarget).closest('.sq-pers-json-list-editor')[0];
        SqPersJLE.drawJsonListEditor(instance, null, true);
    },
    keyvalChange: function(e){
        // if this row is an add row
        row = $(e.currentTarget).closest('.sq-pers-json-keyval');
        if(row.hasClass('sq-pers-json-keyval__add')){
            // if both key and value are changed
            if (row.find('.sq-pers-json__val').val().length > 0 && row.find('.sq-pers-json__val__value').val().length > 0 ){
                // remove add class
                row.removeClass('sq-pers-json-keyval__add');
                // get fieldname
                fieldname = row.find('.sq-pers-json__val').attr('name').split('_')[0];
                // add a new blank row
                row.after(  '<div class="sq-pers-json-keyval sq-pers-json-keyval__add">' +
                            '   <input  class="sq-pers-json__val"' +
                            '           type="text"' +
                            '           value=""' +
                            '           name="' + fieldname + '_key"' +
                            '   />' +
                            '   <input  class="sq-pers-json__val__value"' +
                            '           type="text"' +
                            '           value=""' +
                            '           name="' + fieldname + '_value"' +
                            '   />' +
                            '   <a href="javascript:void(0)">Remove</a>' +
                            '</div>');
                // focus on the new field
                row.next().find('.sq-pers-json__val').focus();
            }
        }
    },
    keyvalRemove: function(e){
        target = $(e.currentTarget).closest('.sq-pers-json-keyval');
        $(target).slideUp(function(){
            $(target).remove();
        });
        SqPersJLE.enableUpdateButton(e);
    },
    enableUpdateButton: function(e){
        $(e.currentTarget).closest('.form-toggle-body').find('.sq-pers-save-row').prop('disabled', false);
    },
    saveRow: function(e){
        wrapper = $(e.currentTarget).closest('.sq-pers-json-list-editor');
        wrapperKeyfield = $(wrapper).data('keyfield');
        row = $(e.currentTarget).closest('.form-toggle-body');
        field = wrapper.find('.sq-pers-json-list-editor__field textarea'); 
        rowkey = $(row).data('key');
        
        // get source data as JSON
        fieldData = field.val();
        data = JSON.parse(fieldData);
        
        rowData = {};
        //if (wrapper.type === "object"){
          
        // recompile the data from the current open row
        row.find('.sq-pers-json-form-group').each(function(key, fieldGroup){
            type = $(fieldGroup).data('type'); 
            key = $(fieldGroup).data('key');
            iskey = $(fieldGroup).data('iskeyfield');
            origvalue = $(fieldGroup).data('origvalue');
            if (iskey == 1){
                rowData.origvalue = origvalue;
            }
            if (type === "text" || type === "color" || type === "json"){
                rowData[key] = $(fieldGroup).find('.sq-pers-json__val').val();
            }else if (type === "select"){
                rowData[key] = $(fieldGroup).find('.sq-pers-json__val').val();
            }else if (type === "keyval"){
                keyVals = {};
                $(fieldGroup).find('.sq-pers-json-keyval').each(function(key,val){
                   inputKey = $(val).find('.sq-pers-json__val').val();
                   inputVal = $(val).find('.sq-pers-json__val__value').val();
                   if (inputKey && inputVal){
                        keyVals[inputKey] = inputVal; 
                   }
                });
                rowData[key] = keyVals;
            }
        });
        
        // Merge the new rows with the data - handling special cases
        
        // if this is an object - as opposed to an array
        if (wrapper.data('type') === "object"){
            // Special case if this is the NEW_FIELD
            if (rowkey === "NEW_FIELD"){
                rowkey = rowData[wrapperKeyfield];
            }
            // Special case if the key changes!!
            if (rowData[wrapperKeyfield] !== rowkey){
                // remove the element at rowkey
                delete(data[rowkey]);
                // set rowkey to rowData[wrapperKeyField]
                rowkey = rowData[wrapperKeyfield];
            }
            
            // update the data object
            data[rowkey] = rowData;
        // this is an array - so loop over it and find the right row to replace
        }else{
            // TODO!
            // for each item in the array
            _.each(data, function(arrayRow,key){
                
                // special case if this is the NEW_FIELD
                if (rowData[wrapperKeyfield] === "NEW_FIELD") {
                    // do nothing becase it just gets set anyway?
                }
                
                // if the key field was updated - we will see 'origvalue'
                if (typeof(rowData.origvalue) !== "undefined"){
                    // if the current data row's key matches the edited row's original key - this is the correct row
                    if (rowData.origvalue === arrayRow[wrapperKeyfield]){
                        // has the key changed?
                        delete(rowData.origvalue);
                        data[key] = rowData;
                    }
                    // TODO - check for clash, e.g. unrelated row is given the same key
                }

            });
        }
        
        // put source data into field
        field.val(JSON.stringify(data));
        
        // enable save button
        $('#ees_saveButtonAction').removeClass('disabled');
        
        // re draw
        SqPersJLE.drawJsonListEditor(wrapper, null, false);
        
    },
    cancelRow: function(e){
        // if this is a new row - remove it
        /*
        instance = $(e.currentTarget).closest('.form-toggle');
        if ($(instance).find('.sq-pers-json__new-row').length > 0){
            
        }*/
        // otherwise: just  redraw
        instance = $(e.currentTarget).closest('.sq-pers-json-list-editor');
        SqPersJLE.drawJsonListEditor(instance, null, false);
    }
}
