// come up with some standard ways to store some user data
// 		get from local storage
//      get from segment etc
//		get from ???

// come up with some standard ways to generate some user data
//		get ip segmentation from funnelback
//		get behaviour tracking from the browser - as per SPF
//			SETUP TRACKING MODULE TO SETUP TRIGGERS TO TRACK ACTIVITY

// some basic user data
userdata = {
    "attributes": {
        "INT_STATUS": ["international"],
        "STUDY_AREA_INTEREST": ["Arts"]
    }
};

// good default ways to get the list from the page
// ???? 
// get the module types and behave differently
// 		e.g.:
//         reorder content OR replace content
//         start with empty OR start with default output
//         load from search OR just filter local content

// get lists from page and action them
sq.personalise.filterPersonalisedAreasInDOM(userdata.attributes, {});

// come up with a testing tool interaction
// 		???

// generic output of user attributes
html = "<h2>User attributes:</h2><ul>"
for (key in userdata.attributes){
	html = html + "<li>";
	html = html + key + ": " + userdata.attributes[key];
	html = html + "</li>";
}
html = html + "</ul>";
$('body').append(html);
