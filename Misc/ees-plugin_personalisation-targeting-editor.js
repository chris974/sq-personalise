/************************************
* PERSONALISATION TARGETING EDITOR PLUGIN
* Chris Grist, Squiz Briz
* Version 0.1.0 - March 2015
* This plugin introduces a custom audience editor for a specific metadata text field, 
* and generates complex audiences which are stored as JSON in the text field. 
* This data is then used to filter the content for personalisation on the front end,
* within the student portal.
*************************************/

/* indexOf polyfill */

// Production steps of ECMA-262, Edition 5, 15.4.4.14
// Reference: http://es5.github.io/#x15.4.4.14
if (!Array.prototype.indexOf) {
  Array.prototype.indexOf = function(searchElement, fromIndex) {
    var k;
    if (this == null) {
      throw new TypeError('"this" is null or not defined');
    }
    var O = Object(this);
    var len = O.length >>> 0;
    if (len === 0) {
      return -1;
    }
    var n = +fromIndex || 0;
    if (Math.abs(n) === Infinity) {
      n = 0;
    }
    if (n >= len) {
      return -1;
    }
    k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);
    while (k < len) {
      if (k in O && O[k] === searchElement) {
        return k;
      }
      k++;
    }
    return -1;
  };
}

/* end polyfill */


GSP_DATA = {};
(function($){
    'use strict';

    /* instance specific configuration */

    var audienceFieldName, audienceFieldID, audienceErrorMessage, audienceLoadingMessage, personalisationAttributeMappingsPath, $currentAttr, $audienceField;
    audienceFieldName = "config.target-audience";
    audienceFieldID = 686577;
    audienceLoadingMessage = "<p><em>Loading...</em><p>";
    audienceErrorMessage = "<p style='color: red;'>Something has gone wrong with trying to process the audience data for this asset.</p>";
    personalisationAttributeMappingsPath = "/student-portal/_designs/pages/personalisation-attribute-mappings"; 

    /* end instance specific configuration */

    EasyEdit.plugins.personalisationTargetingEditor = {
      init: function () {
        var self = this;
        // Add a function to the screen load event
        EasyEditEventManager.bind('EasyEditScreenLoad', function () {
          //check if we are on the metadata screen, if so, call the metadata tab function
          if(EasyEditScreens.getCurrentScreenName() == 'Metadata'){
            initPersonalisationEditor();
          }
        });

        // bind delegated editor launch event
        $('body').on('click', '#edit-audiences-button', renderAudienceEditor);
        $('body').on('click', '.sq-audience-editor__add-attribute', addAnAttribute);
        $('body').on('change', '.sq-edit-audience__select-available-attrs', addAnAttributeSelected);
        $('body').on('click', '.sq-audience-editor__add-audience', addAudienceSelected);
        $('body').on('click', '.sq-edit-audience__remove-attr-link', removeAttr);
        $('body').on('click', '.sq-edit-audience__remove-link', removeAudience); 
      }
    };

    function initPersonalisationEditor(){
        var audienceData, audienceFieldHTML, $audienceOutput;
        $audienceField = $("#metadata_field_text_" + audienceFieldID + "_value");
        // hide the JSON text field    
        $audienceField.hide();
        // add visual area to display the audience summary
        $audienceField.after('<div id="sq-audience-output"></div>');
        $audienceOutput = $('#sq-audience-output');
        // show loading message
        $audienceOutput.html(audienceLoadingMessage);
        // set up script holder and load data for the editing tool        
        $audienceOutput.after('<div id="sq-audience-script"></div>');
        $('#sq-audience-script').load(personalisationAttributeMappingsPath, function(e){
            convertAudienceFieldToAudienceSummary();
        });
    }

    function convertAudienceFieldToAudienceSummary(){
        var $audienceOutput, audienceData;
        $audienceOutput = $('#sq-audience-output');
        audienceData = parseAudience($audienceField.val());
        if (typeof(audienceData) === "string"){
            // if it's a string - it's an error message and we failed to parse the audience JSON
            $audienceOutput.html(audienceErrorMessage);
        }else{
            $audienceOutput.html(renderAudiences(audienceData));
            groupMustOrAudiences();
        }
    }

    function parseAudience(audience){
        var parsedAudience;
        if (typeof(audience) !== "undefined"){
            if (audience.length > 0){
                try {
                    parsedAudience = JSON.parse(audience);
                    return parsedAudience;
                }catch (e){
                    return "Unable to display audience";
                }
            }else{
                return [];
            }
        }else{
            return [];
        }
    }

/* -------- initial display -------- */

    function renderAudiences(audienceData){
        var html, editButton, key;
        html = "<h4>Targeted to:</h4>";
        editButton = "<input type='button' id='edit-audiences-button' value='Edit audiences' />";
        if (audienceData.length < 1){
            // empty audience case
            html = html + "<p>This asset does not currently have any audiences configured.</p>" + editButton;
            return html;
        }else{
            try{
                html = html + "<ul class='sq-audience-list'>";

                for (key in audienceData){
                    html = html + renderAudience(audienceData[key]);
                }
                html = html + "</ul>";
            }catch(e){
                html = html + "<span class='sq-audience-error'>This audience contains field which cannot be found in the field mappings.</span>";
            }
        }
        html = html + editButton;
        return html;
    }
    function friendlyAttrKeys(option, attrKeyList){
        var output, key;
        output = "";
        for (key in attrKeyList){
            output = output + GSP_DATA.personalisationOptions[option].options[attrKeyList[key]] + ", ";
        }
        output = output.replace(/, $/, '');
        return output;
    }
    function formatAudienceDate(date){
        var output, month;
        date = new Date(date);
        month = date.getMonth() + 1;
        output = date.getDate() + "/" + month + "/" + date.getFullYear();
        return output;
    }
    function renderAudience(audience){
        var html, key, friendlyKey, friendlyValue, fieldType, attrKeyList;
        html = "<li class='sq-audience-list__item'><ul>";
        for (key in audience.attr){
            fieldType = GSP_DATA.personalisationOptions[key].type;
            if (fieldType === "multi-select" || fieldType === "select" || fieldType === "date"){
                if (typeof(audience.attr[key]) === "string"){
                    attrKeyList = [audience.attr[key]];
                }else{
                    attrKeyList = audience.attr[key];
                }
                //friendlyValue = GSP_DATA.personalisationOptions[key].options[audience.attr[key]];
                friendlyValue = friendlyAttrKeys(key, attrKeyList);
            }else if (fieldType === "date-range"){
                friendlyValue = "From: " + formatAudienceDate(audience.attr[key].from) + " To: " + formatAudienceDate(audience.attr[key].to);
            }else{
                friendlyValue = audience.attr[key];
            }
            if (typeof(GSP_DATA.personalisationOptions[key]) !== "undefined"){
                friendlyKey = GSP_DATA.personalisationOptions[key].name;
            }else{
                friendlyKey = "<span class='sq-audience-error'>Error finding audience <em>'" + key + "'</em></span>"
                throw "Error finding audience";
            }
            html = html + "<li><span class='sq-audience-init-key'>" + friendlyKey + ":</span> <span class='sq-audience-init-val'>" + friendlyValue + "</span></li>";
        }
        html = html + "</ul><p class='sq-audience-details'><em>"; 
        if (typeof(audience.weight) !== "undefined"){
            html = html + "<div class='sq-audience-details__weight'>weight: " + audience.weight + "</div>"; 
        }
        if (typeof(audience.restrict) !== "undefined"){
            if (audience.restrict === "Must"){
                html = html + "<div class='sq-audience-details__must'>This audience <strong>must</strong> match.</div>";
            }else if (audience.restrict === "MustOR"){
                html = html + "<div class='sq-audience-details__mustor'>One of the audiences in this group must match.</div>";
            }else if (audience.restrict === "Not"){
                html = html + "<div class='sq-audience-details__not'>This audience <strong>must not</strong> match.</div>";
            }
        }
        html = html.replace(/, $/, '');
        html = html + "</em></p></li>";
        return html;
    }
    function groupMustOrAudiences(){
        var $mustOrAudiences;
        $mustOrAudiences = $('.sq-audience-details__mustor').closest('.sq-audience-list__item');
        if ($mustOrAudiences.length > 0){
            $('.sq-audience-list').prepend("<li class='sq-audience-list__item sq-audience-list__item--orgroup'><ul class='sq-audience-list__item--orgroup__list'></ul></li>");
            $('.sq-audience-list__item--orgroup__list').append($mustOrAudiences);
        }
    }


/* -------- editor -------- */


    function renderAudienceEditableWigget(attrKey, attrVal, audienceIndex){
        var html, attr, key, date, checked, falseChecked, from, to;
        
        html = "" + 
        "<li class='sq-audience-editor-attr__item'>" + 
        "   <h5 class='sq-audience-editor-attr__item__key' data-attr='" + attrKey + "' data-type='" + GSP_DATA.personalisationOptions[attrKey].type + "'>" + GSP_DATA.personalisationOptions[attrKey].name  + "</h5>";
        if (typeof(GSP_DATA.personalisationOptions[attrKey].note) === "string"){
            if (GSP_DATA.personalisationOptions[attrKey].note.length > 0){
                html = html + "<div class='sq-edit-audience__note'><em>Note: " + GSP_DATA.personalisationOptions[attrKey].note + "</em></div>";
            }
        }
        if (typeof(GSP_DATA.personalisationOptions[attrKey].example) === "string"){
            if (GSP_DATA.personalisationOptions[attrKey].example.length > 0){
                html = html + "<div class='sq-edit-audience__example'><em>E.g.: " + GSP_DATA.personalisationOptions[attrKey].example + "</em></div>";
            }
        }

        attr = GSP_DATA.personalisationOptions[attrKey];
        switch(attr.type){
            case "select":
            case "multi-select": 
                html = html + "<ul class='sq-edit-audience__checkboxes clearfix'>";
                for (key in attr.options){
                    checked = "";
                    if (key === attrVal){
                        checked = " checked='checked' ";
                    }else if(attrVal.indexOf(key) > -1){
                        checked = " checked='checked' ";
                    }
                    html = html + 
                    "<li>" +
                    "   <input id='sq-attr_" + attrKey + "_" + key + "_" + audienceIndex + "' type='checkbox' name='sq-attr_" + key + "' value='" + key + "' " + checked + "/>" +
                    "   <label for='sq-attr_" + attrKey + "_" + key + "_" + audienceIndex + "' >" + attr.options[key] + "</label>" +
                    "</li>";
                }
                html = html + "</ul>";
                break;
            /*
            case "select":
                html = html + "<select>";
                for (key in attr.options){
                    checked = "";
                    if (key === attrVal){
                        checked = "selected='selected'";
                    }
                    html = html + "<option value='" + key + "' " + checked + ">" + attr.options[key] + "</option>";
                }
                html = html + "</select>";
                break;
            */
            case "boolean":
                checked = "";
                falseChecked = "";
                if (attrVal){
                    checked = "checked='checked'";
                }else{
                    falseChecked = "checked='checked'";
                }

                html = html + 
                "<ul class='sq-edit-audience__checkboxes clearfix'>" +
                "   <li>" +
                "       <input id='sq-attr_" + attrKey + "_true' type='checkbox' name='sq-attr_" + key + "' value='true' " + checked + "/>" +
                "       <label for='sq-attr_" + attrKey + "_true' >True</label>" +
                "   </li>" +
                "   <li>" +
                "       <input id='sq-attr_" + attrKey + "_false' type='checkbox' name='sq-attr_" + key + "' value='false' " + falseChecked + "/>" +
                "       <label for='sq-attr_" + attrKey + "_false' >False</label>" +
                "</li>";                
                html = html + "</ul>";
                break;
            case "text":
                html = html + "<input type='text' value='" + attrVal + "' />";
                break;
            case "date":
                checked = {};
                checked[attrVal] = "checked='checked'";
                html = html + 
                "<select>" + 
                "   <option value='yes' " + checked['yes'] + ">Date is set for student (any value)</option>" + 
                "   <option value='no-date-set' " + checked['no-date-set'] + ">Date is not set for student</option>" + 
                "   <option value='future' " + checked['future'] + ">Date is set and in the future (or today)</option>" + 
                "   <option value='past' " + checked['past'] + ">Date is set and in the past</option>" + 
                "</select>";
                //html = html + "<input type='date' value='" + attrVal + "'/>";
                break;
            case "date-range":
                if (attrVal){
                    if (typeof(attrVal.from) !== "undefined"){
                        from = attrVal.from.replace(/T.*/, '');
                    }else{
                        from = "";
                    }
                    if (typeof(attrVal.to) !== "undefined"){
                        to = attrVal.to.replace(/T.*/, '');
                    }else{
                        to = "";
                    }

                }else{
                    from = "";
                    to = "";
                }
                html = html + 
                "<ul class='sq-edit-audience__daterange'>" +
                "   <input class='from-date' id='sq-attr_" + attrKey + "_from' type='date' value='" + from + "' /><label for='sq-attr_" + attrKey + "_from'>From</label>" + 
                "   <input class='to-date' id='sq-attr_" + attrKey + "_to' type='date' value='" + to + "' /><label for='sq-attr_" + attrKey + "_to'>To</label>" + 
                "</ul>";
                break;
        }

        html = html + "" +
        "    <a class='sq-edit-audience__remove-attr-link'>remove</a>" +
        "</li>";   
        return html;
    }
    function renderAudienceEditable(audience, index, isNew){
        var html, key, restrictSelectedNone, restrictSelectedMust, restrictSelectedMustOR, restrictSelectedNot, weight, newClass;
        restrictSelectedMust = "";
        restrictSelectedNone = "";
        restrictSelectedMustOR = "";
        restrictSelectedNot = "";
        if (audience.restrict === "Must"){
            restrictSelectedMust = "selected='selected'";
        }else if(audience.restrict === "MustOR"){
            restrictSelectedMustOR = "selected='selected'";
        }else if (audience.restrict === "Not"){
            restrictSelectedNot = "selected='selected'";
        }else{
            restrictSelectedNone = "selected='selected'";
        }
        if (typeof(audience.weight) !== "undefined"){
            weight = parseInt(audience.weight);
        }else{ 
            weight = 0;
        }
        if (isNew){
            newClass = "new";
        }else{
            newClass = "";
        }
        html = "" +
        "<li class='sq-edit-audience__item " + newClass + "'>" +
        "   <a class='sq-edit-audience__remove-link' href='javascript:void(0)'>remove</a>" + 
        "   <h3>Audience #" + index + "</h3>" +
        "   <div class='sq-edit-audience__audience'>" +             
        "       <ul class='sq-audience-editor-attr'>";
        for (key in audience.attr){
            html = html + renderAudienceEditableWigget(key, audience.attr[key], index);
        }
        html = html + "</ul>" + 
        "       <input type='button' class='sq-audience-editor__add-attribute' value='Add an attribute' />" +
        "       <div class='sq-edit-audience__weighting'>" +
        "           <label>Weighting: </label>" + 
        "           <input type='text' value='" + weight + "' />" +
        "       </div>" +
        "       <div class='sq-edit-audience__restrict'>" +
        "           <label>Restrict?: </label>" + 
        "           <select>" +
        "               <option " + restrictSelectedNone + " value='0'>Does not need to match</option>" +      
        "               <option " + restrictSelectedMust + " value='Must'>Must match</option>" +
        "               <option " + restrictSelectedNot + " value='Not'>Must not match</option>" +        
        "               <option "  +restrictSelectedMustOR + " value='MustOR'>Must match one of set</option>" +
        "           </select>" + 
        "       </div>" +
        "   </div>" + 
        "</li>";
        return html;
    }



    function renderAudienceEditor(e){
        var audience, audienceFieldTextArea, $audienceField, audienceData, key, message;
        audienceFieldTextArea = "#metadata_field_text_" + audienceFieldID + "_value";
        $audienceField = $(audienceFieldTextArea);        
        audienceData = parseAudience($audienceField.val());

        // ensure that the audience field is not default
        $('#' + $(e.currentTarget).closest('.cellData').find('label.defaultLabel').attr('for')).prop('checked', false);
        $audienceField.prop('disabled', false);

        if (typeof(audienceData) === "string"){
            // error parsing audience
            alert('There is an error with this audience - please contact support.');
        }else{
            message = "<ul class='sq-audience-editor__help sq-audience-editor__interface'>" +
            "   <li>You can add multiple audiences to target this content more selectively</li>" + 
            "   <li>For each audience, you can select multiple attributes (e.g. 'Undergraduate' and 'Nathan campus')</li>" +
            "   <li>For each audience, you can add an optional weighting - the more weightings that match a user, the higher this content will appear in sorted lists (e.g. 'I want to')</li>" +
            "   <li>For each audience, you can select whether this audience <em>must</em> match for the content to be displayed.</li>" + 
            "   <li>Alternatively, you can use 'must match one of set', in which case the user must match at least one of the audiences with this option selected.</li>" +
            "</ul>" +
            "<ul class='sq-edit-audience__list'>";
            for (key in audienceData){
                message = message + renderAudienceEditable(audienceData[key], key, false);
            }
            message = message + 
            "</ul>" +
            "<div class='sq-audience-editor__interface'>" + 
            "   <input type='button' value='Add an audience' class='sq-audience-editor__add-audience' />" +
            "</div>";
            message = message + renderSelectAvailableAttrs();
            

            EasyEditOverlay.showOverlay(
            "confirm", 
            {
                message: message,
                title: "Edit audiences",
                cancelText: "Cancel editing",
                confirmText: "Confirm audience changes",
                cssClass: "sq-audience-editor",
                confirmFn: function() {
                            EasyEditOverlay.hideOverlay();
                            EasyEditOverlay.showOverlay("loading");
                            serialiseAudiences();
                        },
                cancelFn: function() {
                    EasyEditOverlay.hideOverlay();
                }
            });
            fixBubbleHeight();
        }
    }


    function renderSelectAvailableAttrs(){
        var html, key;
        html = "<div class='sq-edit-audience__select-available-attrs' style='display: none;'>" +
        "   <label for='sq-edit-audience__select-available-attrs-select'>Select from the following available attributes:</label>" +
        "   <select id='sq-edit-audience__select-available-attrs-select'>" + 
        "       <option selected='selected' value='0'> -- select from available attribtes -- </option>";
        for (key in GSP_DATA.personalisationOptions){
            html = html + 
            "   <option value='" + key + "'>" + GSP_DATA.personalisationOptions[key].name + "</option>";
        }
        html = html + "   </select>" + 
        "</div>";
        return html;
    }
    function addAnAttribute(e){
        e.preventDefault();
        $('.sq-edit-audience__list').hide();
        $('.sq-audience-editor__interface').hide();
        $('.confirmation.section').hide();
        $('.sq-edit-audience__select-available-attrs').show();
        $currentAttr = $(e.currentTarget).closest('.sq-edit-audience__audience')
        .find('.sq-audience-editor-attr');
        fixBubbleHeight();
    }
    function addAnAttributeSelected(e){
        var selectedKey, alreadyExists;
        alreadyExists = false;
        selectedKey = $(e.target).val();

        // did the select an actual audience?
        if (selectedKey !== 0){
            // does the audience already exist ?
            $currentAttr.find('.sq-audience-editor-attr__item__key').each(function(key,val){
                if ($(val).data('attr') === selectedKey){
                    alreadyExists = true;
                }
            });
            if (alreadyExists){
                alert('This audience already contains an attribute of ' + GSP_DATA.personalisationOptions[selectedKey].name);
            }else{
                $currentAttr.append(renderAudienceEditableWigget(selectedKey, "", $currentAttr.closest('.sq-edit-audience__item').index()));
                setTimeout(function(){
                    $($currentAttr.find('.sq-audience-editor-attr__item:last').find('input,select,textarea,button')[0]).focus();
                }, 1);
                $('.sq-edit-audience__select-available-attrs').hide();
                $('.sq-audience-editor__interface').show();
                $('.sq-edit-audience__list').show();
                $('.confirmation.section').show();
                fixBubbleHeight();
            }
        }
    }
    function addAudienceSelected(e){
        var count, $audience;
        e.preventDefault();
        count = $('.sq-edit-audience__item').length;
        $('.sq-edit-audience__list').append(renderAudienceEditable($(e.target).val(), count, true));
        // add focus to the add attribute button in the new element
        $audience = $('.sq-edit-audience__item:last');
        setTimeout(function(){
            $audience.find('input[type=button]').focus();
            $audience.removeClass('new');
        }, 1);
        fixBubbleHeight();
        return false;
    }


    function removeAttr(e){
        e.preventDefault();
        $(e.currentTarget).closest('.sq-audience-editor-attr__item').remove();
    }
    function removeAudience(e){
        e.preventDefault();
        $(e.currentTarget).closest('.sq-edit-audience__item').remove();
    }

    function fixBubbleHeight(){
        var $bubble, outerHeight, height;
        $bubble = $('.sq-audience-editor .ees_BubbleContent');
        $bubble.css('max-height', '');
        outerHeight = $('.sq-audience-editor').outerHeight();
        if ($bubble.outerHeight() > (($(window).height()/100) * 80) - 50){
            height = $('.sq-audience-editor').outerHeight() - 50;
            $bubble.css('max-height', height + 'px');
            $bubble.css('overflow', 'auto');
        }
    }


/* -------- saving -------- */

    function getAttrValAsStrings(attr){
        var type, attrVal, attrVals, $attr, toDate, fromDate;
        $attr = $(attr);
        type = $attr.find('.sq-audience-editor-attr__item__key').data('type');
        switch(type){
            case "select":
            case "multi-select":
                attrVals = [];
                $attr.find('.sq-edit-audience__checkboxes input:checked').each(function(key,val){
                    attrVals.push($(val).val());
                });
                return attrVals;
                break;
            /*
            case "select":
                attrVal = $attr.find('select').val();
                return attrVal;
                break;
            */
            case "boolean":
                if ($attr.find('input:checked').val() === "true"){
                    return true;
                }else{
                    return false;
                }
                break;
            case "text":
                attrVal = $attr.find('input').val();
                return attrVal;
                break;
            case "date":
                attrVal = $attr.find('select').val();
                return attrVal;
                break;
            case "date-range":
                fromDate = new Date($attr.find('.from-date').val());
                toDate = new Date($attr.find('.to-date').val());
                if (fromDate && toDate){
                    attrVal = {
                        "from": fromDate,
                        "to": toDate
                    };
                }else{
                    attrVal = undefined;
                }
                return attrVal;
                break;
        }
    }

    function serialiseAudiences(){
        var audienceArray, audienceJSON, audienceJSONString;
        audienceArray = [];
        $('.sq-edit-audience__item').each(function(audienceKey, audience){
            var weight, restrict, $audience;
            $audience = $(audience);
            audienceJSON = {};
            audienceJSON.attr = {};
            $(audience).find('.sq-audience-editor-attr__item').each(function(attrIndex, attr){
                var attrKey, attrVal;
                attrKey = $(attr).find('.sq-audience-editor-attr__item__key').data('attr');
                attrVal = getAttrValAsStrings(attr);
                audienceJSON.attr[attrKey] = attrVal;
            });
            weight = parseInt($audience.find('.sq-edit-audience__weighting input').val());
            if (weight){
                audienceJSON.weight = weight;
            }
            restrict = $audience.find('.sq-edit-audience__restrict select').val();
            if (restrict !== "0"){
                audienceJSON.restrict = restrict;
            }
            audienceArray.push(audienceJSON);
        });
        audienceJSONString = JSON.stringify(audienceArray);
        console.log(audienceJSONString);
        $("#metadata_field_text_" + audienceFieldID + "_value").val(audienceJSONString);
        convertAudienceFieldToAudienceSummary();
        EasyEditOverlay.hideOverlay();
        // enable the save button
        $('#ees_saveButtonAction').removeClass('disabled');
        $('#edit-audiences-button').focus();
    }

}(jQuery));