(function (angular) {
    'use strict';
    var module, template, personalisationOptions;
    module = angular.module('gsp-testing-tools', ['gsp-util']);

    //
    //  Testing tools 
    //

    template = 
    "<div data-ng-cloak class='test-panel__outer test-panel__outer--active-{{ sectionsVisible.supportTools }}'>" +
    "    <div class='test-icon'>" +
    "        <a class='test-icon__link fa fa-{{ sectionsVisible.supportTools ? \"caret-left\" : \"gear\" }}' href='javascript:void(0)' data-ng-click='toggleSection(\"supportTools\")'>" +
    "           <span class='visuallyhidden'>Student portal testing tools</span>" +
    "        </a>" + 
    "    </div>" + 
    "    <div class='test-panel visually{{ sectionsVisible.supportTools ? \"visible\" : \"hidden\" }}'>" + 
    "        <h2 class='test-panel__heading'>Student portal test panel</h2>" +
    "        <div class='visually{{ sectionsVisible.buttons ? \"visible\" : \"hidden\" }}' >" +
    "           <a data-ng-if='true || userpersonalisation.canImpersonate' class='test-panel__button' href='javascript:void(0)' data-ng-click='toggleSection(\"impersonateUser\")'>Impersonate user</a>" + 
    "           <a data-ng-if='true || userpersonalisation.canTestPersonalisation' class='test-panel__button' href='javascript:void(0)' data-ng-click='toggleSection(\"modifyAttributes\")'>Modify attributes</a>" + 
    "        </div>" + 
    "        <div class='visually{{ sectionsVisible.buttons ? \"hidden\" : \"visible\" }}' >" +
    "           <a class='test-panel__back' href='javascript:void(0)' data-ng-click='back()'>< back</a>" + 
    "        </div>" +     
    "        <div class='visually{{ sectionsVisible.impersonateUser ? \"visible\" : \"hidden\" }}'>" + 
    "            <div class='visually{{ sectionsVisible.impersonateUserInitial ? \"visible\" : \"hidden\" }}'>" +
    "                <p>Enter S Number to impersonate: </p>" + 
    "                <label for='impersonateUserIDNumber' class='visuallyhidden'>Impersonate user ID number</label><input id='impersonateUserIDNumber' data-ng-model='impersonateUserIDNumber' />" +
    "                <button data-ng-click='impersonate()'>Impersonate</button>" + 
    "            </div>" + 
    "            <div class='visually{{ (sectionsVisible.impersonateUserAfter || userpersonalisation.isImpersonated) ? \"visible\" : \"hidden\" }}'>" +
    "                <p>Currently impersonating: {{ userpersonalisation.sNumber }} </p>" +
    "                <button data-ng-click='unimpersonate()'>Clear impersonation</button>" + 
    "            </div>" + 
    "        </div>" + 
    "        <ul class='test-panel__attributes visually{{ sectionsVisible.modifyAttributes ? \"visible\" : \"hidden\" }}'>" +
    "           <li class='test-panel__attributes__item' data-ng-repeat='(key, value) in personalisationOptions' data-ng-if='value.showInTest === \"Y\"'>" +
    "               <a data-ng-switch on='value.type' class='test-panel__link' href='javascript:void(0)' data-ng-click='toggleSection(\"key--\" + key)'>" + 
    "                   <span class='fa fa-angle-{{ sectionsVisible[\"key--\" + key] ? \"up\" : \"down\"}}'></span><span class='test-panel__label'>{{ value.name }}</span>: " +
    "                   <span data-ng-switch-when='multi-select' class='test-panel__selected-value'>{{ getKeysAsStringWhenTrue(value.value) }}</span>" + 
    "                   <span data-ng-switch-when='text' class='test-panel__selected-value'>{{ value.value }}</span>" + 
    "                   <span data-ng-switch-when='boolean' class='test-panel__selected-value'>{{ value.value }} </span>" + 
    "                   <span data-ng-switch-when='select' class='test-panel__selected-value'>{{ value.value }}</span>" + 
    "                   <span data-ng-switch-when='date-range' class='text-panel__selected-value'>{{ getFriendlyDate(value.value) }}</span>" +    
    "                   <span data-ng-switch-when='date' class='test-panel__selected-value'>{{ getFriendlyDate(value.value) }}</span>" + 
    "               </a>" + 
    "               <div class='test-panel__section visually{{ sectionsVisible[\"key--\" + key] ? \"visible\" : \"hidden\"}}'>" +
    "                   <div data-ng-switch on='value.type'> " +
    "                      <p data-ng-if='value.example' class='test-panel__eg'>E.g: {{ value.example }} </p>" +
    "                      <p data-ng-if='value.note' class='test-panel__note'>Note: {{ value.note }} </p>" + 

// MULTI SELECT

    "                      <ul class='test-panel__option-list' data-ng-switch-when='multi-select'>" + 
    "                          <li data-ng-repeat='(key2, value2) in value.options'>" +
    "                              <input id='{{ value.key + \"-\" + key2 }}' type='checkbox' name='{{ value.key }}' value='{{ key2 }}' data-ng-model='value.value[key2]' data-ng-change='updatePersonalisationValues()' />" +
    "                              <label for='{{ value.key + \"-\" + key2 }}'>{{ value2 }}</label>" + 
    "                          </li>" + 
    "                      </ul>" + 

// TEXT

    "                      <div data-ng-switch-when='text'>" +
    "                           <label class='visuallyhidden' for='testing-tool-field-{{ key }}'>{{ key }}</label>" + 
    "                           <input type='text' id='testingtool-field-{{ key }}' data-ng-model='value.value'  data-ng-change='updatePersonalisationValues()' />" +
    "                      </div>" +    

// BOOLEAN

    "                      <div data-ng-switch-when='boolean'>" +
    "                          <input id='testing-tool-field-{{ value.key }}' type='checkbox' name='{{ value.key }}' data-ng-model='value.value'  data-ng-change='updatePersonalisationValues()' />" + 
    "                          <label for='testing-tool-field-{{ value.key }}'>True</label>" +
/*    "                           <select data-ng-model='value.booleanTemp' data-ng-change='booleanChange({key: value.key, value: value})' id='testing-tool-field-{{ key }}' data-ng-model='value.value' data-ng-change='updatePersonalisationValues()'>" +
    "                               <option value='null' > -- empty -- </option>" + 
    "                               <option value='true' >True</option>" + 
    "                               <option value='false' >False</option>" + 
    "                           </select>" +
    "                          <label for='testing-tool-field-{{ value.key }}'>True, False, or not set</label>" +
*/
    "                      </div>" +
    
// SELECT

    "                      <div data-ng-switch-when='select'>" +
    "                           <label for='testing-tool-field-{{ key }}'>{{ key }}</label>" + 
    "                           <select id='testing-tool-field-{{ key }}' data-ng-model='value.value' data-ng-change='updatePersonalisationValues()'>" +
    "                               <option data-ng-repeat='(key2, value2) in value.options' value='{{ key2 }}' >{{ value2 }}</option>" + 
    "                           </select>" +
    "                      </div>" +

// DATE

    "                      <div data-ng-switch-when='date'>" +
    "                           <label class='visuallyhidden' for='testing-tool-field-{{ key }}'>{{ key }}</label>" + 
    "                           <input type='date' id='testingtool-field-{{ key }}' data-ng-model='value.value' data-ng-change='updatePersonalisationValues()'>" +
    "                      </div>" + 
// DATE RANGE (e.g. set the current date in testing tool)
    "                      <div data-ng-switch-when='date-range'>" +
    "                           <label class='visuallyhidden' for='testing-tool-field-{{ key }}'>{{ key }}</label>" +     
    "                           <input type='date' id='testingtool-field-{{ key }}' data-ng-model='value.value' data-ng-change='updatePersonalisationValues()'>" +
    "                      </div>" + 
    "                      <div class='test-panel__suppress'> " +
    "                           <input id='testing-tool-{{ value.key }}-suppress' type='checkbox' name='{{ value.key }}-suppress' data-ng-model='value.suppress' data-ng-change='updatePersonalisationValues()' />" +
    "                           <label for='testing-tool-{{ value.key }}-suppress' >Suppress this field (simulate not set)</label>" +
    "                      </div>" +
    "               </div>" +
    "           </li>" +
    "        </ul>" +
    "        <p>{{ items }}</p>" +
    "    </div>" +
    "</div>";

    module.directive('testPanel', ['GSPUtil', function(GSPUtil) {
        var directive;
        directive = {
            restrict: 'A',
            replace: true,
            link: function($scope, element, attrs){
                var key2, personalisationOptions, personalisationValues;
                $scope.supportToolsVisible = 'none';
                $scope.modifyAttributesVisible = 'none';
                $scope.impersonateUserVisible = 'none';
                personalisationOptions = GSP_DATA.personalisationOptions;       
                /*
                $scope.booleanChange = function(options){
                    console.log('boolean change...')
                    console.log(options.key);
                    if (typeof(options.value.booleanTemp) !== "undefined"){
                        if (options.value.booleanTemp === "true"){
                            $scope.personalisationOptions[options.key] = true;
                        }else if (options.value.booleanTemp === "false"){
                            $scope.personalisationOptions[options.key] = false;
                        }else{
                            delete($scope.personalisationOptions[options.key]);
                            console.log('deleting!');
                        }
                    }else{
                        delete($scope.personalisationOptions[options.key]);
                        console.log('deleting!');
                    }
                };
                */

                $scope.$on("loaded.all", function(newValue, oldValue){
                    console.log('[watch]: test panel loaded.all');
                    if ($scope.loaded.all){
                        var mergedPersonalisationOptions;
                        mergedPersonalisationOptions = GSPUtil.mergePersonalisationOptions(personalisationOptions, $scope.userpersonalisation);
                        $scope.personalisationOptions = mergedPersonalisationOptions;
                        $scope.sectionsVisible = {
                            modifyAttributes: false,
                            impersonateUser: false,
                            supportTools: false,
                            buttons: true,
                            impersonateUserInitial: true
                        }
                    }
                });
                $scope.$on("switched.primary-campus", function(){
                    console.log('Primary campus swithced in testing tools!');
                    // update testing tool...
                    $scope.personalisationOptions = GSPUtil.mergePersonalisationOptions(personalisationOptions, $scope.userpersonalisation);
                });

                $scope.$on('loaded.all', function(){
                    console.log('TESTING TOOLS LOADED ALL EVENT');
                });


                $scope.$on('open.testtools', function(){
                    $scope.toggleSection("supportTools");
                });
                $scope.togglePanel = function(){
                    GSPUtil.togglePanel($scope)
                };

            }
        };
        directive.template = template;
        return directive;
    }]);

    //test panel controller
    module.controller('testPanelController', ['$scope', 'GSPUtil', 'GriffithApi', function($scope, GSPUtil, GriffithApi){

        $scope.updatePersonalisationValues = function(){
            setTimeout(function(){
                $scope.$apply(function(){
                    $scope.userpersonalisation['attributes'] = GSPUtil.unmergePersonalisationOptions($scope.personalisationOptions);
                });
            }, 0);
        };

        $scope.impersonate = function(){
            if (!$scope.impersonateUserIDNumber.match(/^s/)){
                $scope.impersonateUserIDNumber = 's' + $scope.impersonateUserIDNumber;
            }
            GriffithApi.auth.beginImpersonateUser($scope.impersonateUserIDNumber).then(function(){
                var mergedPersonalisationOptions;
                console.log('impersonation success');
                $scope.loadPageData({"clearCache": true});
                $scope.sectionsVisible.impersonateUserInitial = false;
                $scope.sectionsVisible.impersonateUserAfter = true;
                // clear image backgrounds
                $scope.reloadPageDataSingular('getimagekeys,1');
                // update the testing tool's details
                mergedPersonalisationOptions = GSPUtil.mergePersonalisationOptions(personalisationOptions, $scope.userpersonalisation);
                $scope.personalisationOptions = mergedPersonalisationOptions;
            },function(){
                alert('Impersonation failed');
            });
        };

        $scope.unimpersonate = function(){
            GriffithApi.auth.endImpersonateUser();
            $scope.loadPageData({"clearCache": true});
            $scope.sectionsVisible.impersonateUserInitial = true;
            $scope.sectionsVisible.impersonateUserAfter = false;            
        };

        $scope.toggleSection = function(section){
            var keyParts, key;
            // general case to toggle visibility
            $scope.sectionsVisible[section] = !$scope.sectionsVisible[section];
            // specific case to hide buttons if either sub section is shown
            if ($scope.sectionsVisible.impersonateUser || $scope.sectionsVisible.modifyAttributes){
                $scope.sectionsVisible.buttons = false;
            }
            // if the key has a double hyphen - close other keys matching that
            keyParts = section.split('--');
            if (keyParts.length > 1){
                for (key in $scope.sectionsVisible){
                    if (!(key === section)){
                        if (key.match(keyParts[0])){
                            $scope.sectionsVisible[key] = false;
                        }
                    }
                }
            }
        };
        $scope.back = function(){
            $scope.sectionsVisible.buttons = true;
            $scope.sectionsVisible.impersonateUser = false;
            $scope.sectionsVisible.modifyAttributes = false;
        };
        $scope.getKeysAsStringWhenTrue = function(object){
            var string, key;
            string = "";
            for (key in object){
                if (object[key]){
                    string = string + key + ", ";
                }
            }
            if (string.length > 0){
                string = string.replace(/, $/, '');
            }
            return string;
        }
        $scope.getFriendlyDate = function(date){
            if (typeof(date) !== "undefined" && date){
                if (date.toDateString() === "Invalid Date"){
                    return "No date set";
                }else{
                    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
                }
            }else{
                return "";
            }
        }



    }]);

}(angular));

