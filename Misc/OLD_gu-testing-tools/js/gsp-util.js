(function (angular) {
    'use strict';
    var module;
    module = angular.module('gsp-util', ['gspSessionStorage', 'griffith-api-gatekeeper']);

    // 
    //  Griffith Student Portal global custom directives
    // 

    module.directive('onLastRepeat', ['$timeout', function($timeout) {
        return function($scope, element, attrs) {
            /*
            if ($scope.$last) setTimeout(function(){
                $scope.$emit('onRepeatLast', element, attrs);
            }, 1);
            */
            if ($scope.$last) {
                $timeout(function () {
                    $scope.$emit('onRepeatLast', element, attrs);
                });
            }
        };
    }])

    //
    //  Griffith Student Portal utilities factory
    //

    module.factory('GSPUtil', ['$window', 'GSPSessionStorage', 'GriffithAuth', function ($window, GSPSessionStorage, GriffithAuth){
        /*  getWidth
            for responsive resizing
        */
        function getWidth(){
            var width = $('#width-indicator').css('z-index');
            switch(width){
                case "1":
                    return "mobile";
                break;
                case "2":
                    return "tablet";
                break;
                case "3":
                    return "desktop";
                break;
                default:
                break;
            }
        }

        function setLightboxContentHeight(){
            var $wrapper, $inner, $header, $footer, innerHeight, wrapperHeight;
            $wrapper = $('.popup__wrapper');
            $header = $('.popup__header');
            $footer = $('.popup__footer');
            $inner = $('.popup__content');
            wrapperHeight = $(window).height() - 40;
            if ($wrapper.height() > wrapperHeight){
                $wrapper.height(wrapperHeight);
                innerHeight = $wrapper.outerHeight() - $header.outerHeight() - $footer.outerHeight();
                if ($inner.height() > innerHeight){ 
                    $inner.css('max-height', innerHeight).css('overflow', 'auto');    
                }else{
                    $inner.css('max-height', 'inherit');
                }
            }
            
        }

        // toggles panels
        function togglePanel($scope){
            if (typeof($scope.activeClass) !== "undefined"){
                if ($scope.activeClass.length > 0){
                    $scope.activeClass = "";
                }else{
                    $scope.activeClass = "panel__inner--active";
                }
            }else{
                $scope.activeClass = "panel__inner--active";
            }   
        }


        // Prep data sources for initial load of data
        /*
            This function takes the data source supplied by the page (e.g. my courses), and it's relevant map to 
            determine the final data source path
        */         
        function prepDataSources(key){
            var dataSources, dataSourceKeys, dataSourceAges, dataSourceStrings, dataSourceMapping;

            /*
                Main data source mapping - consider moving
            */
            dataSourceMapping = {
                getmycourses: {
                    age: 3600,
                    path: ['mycourses', '/v##']
                },
                getlatestmessages: {
                    age: 7200,
                    path: ['mymessages', '/v##', '/unread?num=##']
                },
                getallmessages: {
                    age: 7200,
                    path: ['mymessages', '/v##', '/all?days=##']
                },
                getallstorage: {
                    age: 164300,
                    path: ['storage', '/v##']
                },
                getimagekeys: {
                    age: 1,
                    path: ['photostore', '/v##/keys']
                },
                getmydetails: {
                    age: 7200,
                    path: ['mydetails', '/v##']
                },
                getmyschedule: {
                    age: 7200,
                    path: ['myschedule', '/v##']
                },
                getmyresults: {
                    age: 3600,
                    path: ['myresults', '/v##']
                }
            };


            dataSourceStrings = $('#page-wrapper').data('dataSources').split(';')
            // override if we passed a key
            if (key){
                // just load this one data source - specified by parameter rather than page wrapper data
                dataSourceStrings = [key];
                dataSources = [];
                dataSourceKeys = [];
                dataSourceAges = [];
            }else{
                // user personalisation query is a requirement for all pages, unless we're reloading one feed by passing a key
                dataSources = [
                    "userpersonalisation/v1"
                ];
                dataSourceKeys = [
                    "userpersonalisation"
                ];
                dataSourceAges = [
                    3600
                ];
            }
            $(dataSourceStrings).each(function(key,val){
                var source, result;
                source = $.trim(val).split(',');
                if (source[0].length > 0){
                    result = prepDataSource(source, dataSourceMapping[source[0]].path);
                    dataSources.push(result);
                    dataSourceKeys.push(source[0]);
                    dataSourceAges.push(dataSourceMapping[source[0]].age);
                }
            });
            return {
                "paths": dataSources,
                "keys": dataSourceKeys,
                "ages": dataSourceAges
            }
        }
        // private function
        function prepDataSource(source, mapping){
            var sourceString;
            sourceString = mapping[0];
            $(source).each(function(key,val){
                if (key > 0){
                    sourceString = sourceString + mapping[key].replace('##', val);
                }
            });
            return sourceString;
        }

        // adds personalisation values to the personalisation schema
        function mergePersonalisationOptions(options, values){
            var key, value, arrayIndex;
            for (key in options){
                if (options[key].type === "date"){
                    value = new Date(values['attributes'][key]);
                // if value is string
                }else if(options[key].type === "multi-select" && typeof(values['attributes'][key]) === "string"){
                    // if a string was passed to a multi-select type... make it a one element array
                    value = {};
                    value[values['attributes'][key]] = true;
                // if value is array
                }else if(options[key].type === "multi-select" && angular.isArray(values['attributes'][key])){
                    // if an array was passed to multi-select type - convert it to an object
                    value = {};
                    for (arrayIndex in values['attributes'][key]){
                        value[values['attributes'][key][arrayIndex]] = true;
                    }
                // if value is object
                }else if(options[key].type === "multi-select" && typeof(values['attributes'][key] === "object")){
                    value = values['attributes'][key];
                }else{
                    value = values['attributes'][key];
                }
                options[key].value = value;
            }
            return options;
        }

        // seperates personalisation values from the testing tool data
        function unmergePersonalisationOptions(options){
            var values, key;
            values = {};
            for (key in options){
                if (options[key].value){
                    values[key] = options[key].value;
                }
            }
            // remove any suppressed fields
            for (key in options){
                if (options[key].suppress){
                    delete(values[key]);
                }
            }
            return values;
        }

        // convert datetime to play nicely in Safari browsers
        function getCompatibleDates(timeString) {
            /* 
             Safari cannot process dates that have "2015-03-30T08:30:11+0000"
             But can process if it was "2015/03/30 08:30:11 +0000"

             IE9 cannot process dates like "Tue Mar 31 02:05:08 +0000 2015"
             But can process if it was "Tue Mar 31 2015 02:05:08 +0000"
            */

            var compatibleDatetime = timeString;
            var matchISOFormat = timeString.match(/([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}\:[0-9]{2}\:[0-9]{2})([+-]?[0-9]{4})/);
            var matchFriendlyFormat = timeString.match(/([a-zA-Z]{3}\ [a-zA-Z]{3}\ [0-9]{1,2})\ ([0-9]{2}\:[0-9]{2}\:[0-9]{2}\ [+-]?[0-9]{4})\ ([0-9]{4})/);

            if(matchISOFormat !== null) {
                compatibleDatetime = matchISOFormat[1].replace(/-/g, '/') + ' ' + matchISOFormat[2] + ' ' + matchISOFormat[3];
            }

            if(matchFriendlyFormat !== null) {
                compatibleDatetime = matchFriendlyFormat[1] + ' ' + matchFriendlyFormat[3] + ' ' + matchFriendlyFormat[2];
            }

            return compatibleDatetime;
        }

        // take a datetime and show time difference
        function getRelativeTime(guTime){
            var system_date = new Date(getCompatibleDates(guTime));
            var user_date = new Date();
            var diff = Math.floor((user_date - system_date) / 1000);
            if (diff <= 1) {return "just now";}
            if (diff < 60) {return diff + "s";}
            if (diff <= 3540) {return Math.round(diff / 60) + "m";}
            if (diff <= 5400) {return "1h";}
            if (diff <= 86400) {return Math.round(diff / 3600) + "h";}
            if (diff <= 129600) {return "1d";}
            if (diff < 604800) {return Math.round(diff / 86400) + "d";}
            if (diff <= 777600) {return "1w";}
            if (diff > 604800) {return Math.round(diff / 604800) + "w";}

            return "on " + system_date;
        }
  
        // Return the API for this service.
        return angular.extend({
            // Determine if the user is currently authenticated
            getWidth: getWidth,
            // navSliderResize: navSliderResize,
            togglePanel: togglePanel,
            prepDataSources: prepDataSources,
            mergePersonalisationOptions: mergePersonalisationOptions,
            unmergePersonalisationOptions: unmergePersonalisationOptions,
            getRelativeTime: getRelativeTime,
            setLightboxContentHeight: setLightboxContentHeight,
            getCompatibleDates: getCompatibleDates
        }, GSPSessionStorage);
    }]);


}(angular));