# Installation

* import container templates
* Set up git bridge - or at least load in the CSS, JS and other static files
    * /CSS/*
    * /JS/* 
    * /lib/*
    * /Templates/*
    * /Editing/CSS/*
    * /Editing/JS/*
    * /Editing/Images/*
* create Edit+ include from example
* create footer JS include from example
    * invoke personalisation function