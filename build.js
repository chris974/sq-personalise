var buildify = require('buildify');

// front end

buildify()
    .load('src/JS/sq-personalise.js')
    .concat('src/JS/sq-personalise-track.js')
    .save('/dist/JS/sq-personalise.js');

buildify()
    .load('src/CSS/sq-personalise.css')
    .save('/dist/CSS/sq-personalise.css');


// edit plugins
 
buildify()
  .load('src/edit/JS/base.js')
  .concat([
    'src/edit/JS/cct-xp__plugin__boolean-switcher.js', 
    'src/edit/JS/cct-xp__plugin__conditional-fields.js', 
    'src/edit/JS/cct-xp__plugin__toggle.js', 
    'src/edit/JS/plugin-lib-pers-data.js', 
    'src/edit/JS/plugin-pers-audience-editor.js', 
    'src/edit/JS/plugin-pers-color-input.js', 
    'src/edit/JS/plugin-image-related-asset.js', 
    'src/edit/JS/plugin-pers-json-list-editor.js', 
    'src/edit/JS/plugin-pers-json-list-editor_persona-traits.js', 
    'src/edit/JS/plugin-pers-query-builder.js', 
    'src/edit/JS/plugin-pers-testing-tool.js'
  ])
  .save('/dist/edit/sq-pers-edit-plugins.js');

buildify()
    .load('src/edit/CSS/base.css')
    .concat([
        'src/edit/CSS/cct-xp__backend__component-tabs.css',
        'src/edit/CSS/cct-xp__backend__component-template-layout.css',
        'src/edit/CSS/cct-xp__backend__component-toggle.css',
        'src/edit/CSS/cct-xp__plugin__boolean-switcher.css',
        'src/edit/CSS/cct-xp__plugin__conditional-fields.css',
        'src/edit/CSS/cct-xp__plugin__toggle.css',

        'src/edit/CSS/plugin-image-related-asset.css',
        'src/edit/CSS/plugin-pers-audience-editor.css',
        'src/edit/CSS/plugin-pers-json-list-editor.css',
        'src/edit/CSS/plugin-pers-testing-tool.css',
        'src/edit/CSS/plugin-query-builder.css',
    ])
    .save('/dist/edit/sq-pers-edit-plugins.css');

// templates

buildify()
    .load('src/edit/Templates/base.html')
    .concat([
        'src/edit/Templates/add-audience-matching-rule.html',
        'src/edit/Templates/add-query-builder.html',
        'src/edit/Templates/audience-editor.html',
        'src/edit/Templates/json-list-editor.html',
        'src/edit/Templates/query-editor.html',
        'src/edit/Templates/testing-tool.html',
        'src/edit/Templates/persona-selector.html',
    ])
    .save('/dist/edit/sq-pers-templates.html');